clc; clearvars; close all; 

%% Initialize scenario
cd scenarios;
%scen = TB3_scenario_MDP;
%scen = husky_scenario_MDP;
%scen = collision_scenario_collab;
scen = environment2;
cd ..;
scenario = scenarioClass(scen);

% Initialize dagents
cd parameters;
%par = turtlebot3_params("burguer_double_integrator");
%par = husky_params("husky_double_integrator");
par = generic_robot_params("type_1");
par_expert = human_params("average");
cd ..;

for i = 1 : scenario.number_agents-1; agent(i) = AgentClass(par); end
agent(scenario.number_agents) = AgentClass(par_expert);
   
% Build polytopes
polytopes = PolytopeClass(agent,scenario);

analysis = AnalysisClass(scenario,polytopes);
analysis.BuildColors()
analysis.PlotInMPDGrid(1)

%% Initialization of external agent

% In this code I'll generate a nominal trajectory of the expert and 2 extra
% possibilities. I'll try to switch them in real-time to see how the MPC
% reacts to it

% ENVIRONMENT 1
%pred_grid_traj{1} = {[0,2],[1,2],[2,2],[2,3],[2,4],[3,4],[3,4],[3,3],[3,2],[3,1],[4,1],[4,0],[5,0],[5,0],[5,1]};
%real_grid_traj{1} = {[0,2],[1,2],[2,2],[2,3],[2,4],[3,4],[3,4],[3,3],[3,2],[3,1],[4,1],[4,0],[5,0],[5,0],[5,1]};

% ENVIRONMENT 2
pred_grid_traj{1} = {[1,0],[1,1],[1,2],[1,3],[1,4],[1,4],[2,4],[3,4],[3,3],[3,2],[3,1],[3,1],[4,1],[5,1]};
real_grid_traj{1} = {[1,0],[1,1],[1,2],[1,3],[1,4],[1,4],[2,4],[3,4],[3,3],[3,2],[3,1],[3,1],[4,1],[5,1]};

%% Build MAS MPC
MIP = MIPClass(agent,polytopes);
MIP.BuildOptVarIndexes();
[constraints, cost, options] = MIP.BuildOptProblem(agent,polytopes);
MIP.InitializeOpt(constraints,cost,options); % Initialize optimizer object for closed-lopp

pred_cont_traj = MIP.Discrete2ContinuousTraj(pred_grid_traj,scenario);
real_cont_traj = MIP.Discrete2ContinuousTraj(real_grid_traj,scenario);

%% Initialization of simulation

% visited targets
visitedTargets = zeros(scenario.number_tar,1);

% state and input
x_feedback = []; 
for i = 1 : scenario.number_agents
    agent(i).InitializeVars(MIP,scenario.ini_state(:,i));
    
    if i < scenario.number_agents % last agetn is external
        x_feedback = [x_feedback;scenario.ini_state(:,i)];
    end
end

%% Simulation loop
for k = 1 : MIP.N
    disp("Targets visited:")
    disp(visitedTargets(1:end-1)');
    
    % recedes horizon of cont_traj
    hor_human = length(pred_grid_traj{1})-(k-1); 
    cont_traj_fb = [pred_cont_traj{1}((k-1)*2+1:end)];
    lines2add = length(MIP.opt_vars.exp_traj) - length(cont_traj_fb);
    cont_traj_fb = [cont_traj_fb;repmat(pred_cont_traj{1}(end-1:end),[lines2add/2,1])];
    
    % Optimization
    [sol,optTime,sol_flag] = MIP.Optimize(x_feedback,visitedTargets(1:end-1),cont_traj_fb,hor_human);
    
    analysis.SaveOptSolution(sol,k); % Save opt 

    x_feedback = [];
    for i = 1 : scenario.number_agents   

        % Robot dynamics
        if i < scenario.number_agents
            agent(i).vars.u(:,k) = analysis.mip_sol{k}.U(MIP.index.u(:,1,i));
            agent(i).vars.x(:,k+1) = agent(i).robot_dynamics(agent(i).vars.x(:,k),agent(i).vars.u(:,k));
            x_feedback = [x_feedback;agent(i).vars.x(:,k+1)];
             
        % External agent dynamics
        else
            ind = [((k+1)-1)*2+1,2*(k+1)];
            agent(i).vars.x(:,k+1) = [real_cont_traj{1}(ind(1)); 0; real_cont_traj{1}(ind(2)); 0];
        end
        visitedTargets = agent(i).UpdateTargetVisitation(visitedTargets,polytopes,k+1);
        if visitedTargets(end); break; end

    end
    
    if visitedTargets(end); break; end
end

N_opt = k+1;
%% Analysis
close all;

analysis.BuildColors()
analysis.ProcessResults(agent,N_opt)
%analysis.PlotTrajectories()
analysis.PlotSnapshots()


%% Checking first solution
Nopt = find(analysis.mip_sol{1}.B_hor==1);

v = 1;
analysis.PlotInMPDGrid(0)
for i = 1 : 2
    rx = analysis.mip_sol{v}.X(MIP.index.x(1,1:Nopt,i));
    ry = analysis.mip_sol{v}.X(MIP.index.x(3,1:Nopt,i));
    plot(rx,ry,'--o')
end

rx_ext = analysis.mip_sol{v}.X_ext(MIP.index.x_ext(1,1:Nopt));
ry_ext = analysis.mip_sol{v}.X_ext(MIP.index.x_ext(2,1:Nopt));
plot(rx_ext,ry_ext,'--o')


ctrl=0;
for k = 1 : Nopt
   ctrl = ctrl + norm(analysis.mip_sol{v}.U(MIP.index.u(:,k,1)),1) ;
    
end
