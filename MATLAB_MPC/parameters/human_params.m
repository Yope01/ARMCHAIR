function human_pars = human_params(type)


    if lower(type) == "average"
         human_pars.length = 0.290;
         human_pars.width = 0.410;

        human_pars.max_lin_vel = 1.0;
        human_pars.min_lin_vel = 0;
        human_pars.max_acc = 1.0;
        human_pars.min_acc = -1.0;
        human_pars.conn_region_radius = 2.5;
        
        % Dynamics approximated to DI
        human_pars.dyn.A = [0 1 0 0;0 0 0 0;0 0 0 1;0 0 0 0];
        human_pars.dyn.B = [0 0; 1 0; 0 0; 0 1];
        human_pars.dyn.C = eye(4);
        human_pars.dyn.D = zeros(4,2);
        
        % Discretization
        Ts = 1;
        [Ad,Bd,Cd,Dd] = c2dm(human_pars.dyn.A,human_pars.dyn.B,human_pars.dyn.C,human_pars.dyn.D,Ts);
        human_pars.dyn.Ad = Ad;
        human_pars.dyn.Bd = Bd;
        human_pars.dyn.Cd = Cd;
        human_pars.dyn.Dd = Dd; 
    else
        human_pars = [];
        disp("Wrong agent type");
    end


end