function tb3_pars = turtlebot3_params(type)


    if lower(type) == "burguer_double_integrator"
        tb3_pars.length = 0.138;
        tb3_pars.width = 0.178;
        tb3_pars.max_lin_vel = 0.22;
        tb3_pars.min_lin_vel = 0;
        tb3_pars.max_acc = 0.22;
        tb3_pars.min_acc = -0.22;
        tb3_pars.conn_region_radius = 1.5;
        
        % Dynamics approximated to DI
        tb3_pars.dyn.A = [0 1 0 0;0 0 0 0;0 0 0 1;0 0 0 0];
        tb3_pars.dyn.B = [0 0; 1 0; 0 0; 0 1];
        tb3_pars.dyn.C = eye(4);
        tb3_pars.dyn.D = zeros(4,2);
        
        % Discretization
        Ts = 4;
        [Ad,Bd,Cd,Dd] = c2dm(tb3_pars.dyn.A,tb3_pars.dyn.B,tb3_pars.dyn.C,tb3_pars.dyn.D,Ts);
        tb3_pars.dyn.Ad = Ad;
        tb3_pars.dyn.Bd = Bd;
        tb3_pars.dyn.Cd = Cd;
        tb3_pars.dyn.Dd = Dd; 
    else
        tb3_pars = [];
        disp("Wrong agent type");
    end


end