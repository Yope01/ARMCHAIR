function gen_robot_pars = generic_robot_params(type)


    if lower(type) == "type_1"
        gen_robot_pars.length = 0.25;
        gen_robot_pars.width = 0.25;
        gen_robot_pars.max_lin_vel = 2.5;
        gen_robot_pars.min_lin_vel = 0;
        gen_robot_pars.max_acc = 5.0;
        gen_robot_pars.min_acc = -5.0;
        gen_robot_pars.conn_region_radius = 4;
        
        % Dynamics approximated to DI
        gen_robot_pars.dyn.A = [0 1 0 0;0 0 0 0;0 0 0 1;0 0 0 0];
        gen_robot_pars.dyn.B = [0 0; 1 0; 0 0; 0 1];
        gen_robot_pars.dyn.C = eye(4);
        gen_robot_pars.dyn.D = zeros(4,2);
        
        % Discretization
        Ts = 1;
        [Ad,Bd,Cd,Dd] = c2dm(gen_robot_pars.dyn.A,gen_robot_pars.dyn.B,gen_robot_pars.dyn.C,gen_robot_pars.dyn.D,Ts);
        gen_robot_pars.dyn.Ad = Ad;
        gen_robot_pars.dyn.Bd = Bd;
        gen_robot_pars.dyn.Cd = Cd;
        gen_robot_pars.dyn.Dd = Dd; 
    else
        gen_robot_pars = [];
        disp("Wrong agent type");
    end


end