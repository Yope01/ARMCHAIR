classdef AnalysisClass < handle

    properties
        mip = [];
        scenario;
        polytopes;
        traj = [];
        vel = [];
        acc = [];
        colors = [];
        agents = [];
        mip_sol = []
        number_agents;
        N_opt;
    end
    
    methods
        %%%%%%%%%
        function obj = AnalysisClass(scenario,polytopes)
            obj.scenario = scenario;
            obj.polytopes = polytopes;
        end
        
        %%%%%%%%%
        function ProcessResults(obj,agents,N_opt)
            obj.agents = agents;
            obj.number_agents = length(agents);
            obj.N_opt = N_opt;
        end
        
        %%%%%%%%%
        function SaveOptSolution(obj,sol,k)
                       
            obj.mip_sol{k}.X = sol{1};
            obj.mip_sol{k}.X_ext = sol{2};
            obj.mip_sol{k}.U = sol{3};
            obj.mip_sol{k}.Abs_U = sol{4};
            obj.mip_sol{k}.B_tar = sol{5};
            obj.mip_sol{k}.B_obs = sol{6};
            obj.mip_sol{k}.B_col = sol{7};
            obj.mip_sol{k}.B_hor = sol{8};
            obj.mip_sol{k}.x_fb = sol{9};
            obj.mip_sol{k}.wasTargetVisited = sol{10};
            
        end
        
        %%%%%%%%%
        function PlotTrajectories(obj)
            
            obj.PlotInMPDGrid(0);
            for i = 1 : obj.number_agents
                plot(obj.agents(i).vars.x(1,1:obj.N_opt),obj.agents(i).vars.x(3,1:obj.N_opt),'color',obj.colors{i},'LineWidth',1.5);
            end
            
        end

        %%%%%%%%%
        function BuildColors(obj)
            
            obj.colors = {'r','g','b','m','y'};
           
        end
        
        %%%%%%%%%
        function PlotInMPDGrid(obj,plot_robot_bodies)
            
            % Plot operational region
            fig = figure(); hold on;
            field_skinny = Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q);
            poly_robot_body = Polyhedron(obj.polytopes.agent_body{1}.P,obj.polytopes.agent_body{1}.q);
            field = field_skinny + poly_robot_body;
            
            field.plot('color','white','alpha',0.2);
            
%             x_min = -obj.polytopes.op_region.q(3);
%             y_min = -obj.polytopes.op_region.q(4);
%             x_max =  obj.polytopes.op_region.q(1);
%             y_max =  obj.polytopes.op_region.q(2);
            x_min = -obj.scenario.op_region_size_x/2.0;
            x_max =  obj.scenario.op_region_size_x/2.0;  
            y_min = -obj.scenario.op_region_size_y/2.0;
            y_max =  obj.scenario.op_region_size_y/2.0;  

            
%             % Plot obstacles
            for i=1:obj.scenario.number_obs
                
                % Remove robot body margin
                poly_obs_fat = Polyhedron(obj.polytopes.obs{i}.P,obj.polytopes.obs{i}.q);
                poly_obs = poly_obs_fat - poly_robot_body;
                
                %plot(Polyhedron(obj.polytopes.obs{i}.P,obj.polytopes.obs{i}.q),'color','black');
                plot(poly_obs,'color','black');
            end
%             
            % Plot targets
            for i=1:obj.scenario.number_tar-1
                plot(Polyhedron(obj.polytopes.tar{i}.P,obj.polytopes.tar{i}.q),'color','black','alpha',0.2);
            end
            plot(Polyhedron(obj.polytopes.tar{obj.scenario.number_tar}.P,obj.polytopes.tar{obj.scenario.number_tar}.q),'color','magenta','alpha',0.5,'linewidth',0.001);
%             
            % Initial positions
            for i = 1 : obj.scenario.number_agents
                
                
                % Plot robot bodies?
                if plot_robot_bodies
                    q = [obj.scenario.ini_state(1,i);
                         obj.scenario.ini_state(3,i);
                        -obj.scenario.ini_state(1,i);
                        -obj.scenario.ini_state(3,i)]+...
                        [obj.polytopes.agent_body{i}.q(1);
                        obj.polytopes.agent_body{i}.q(2);
                        obj.polytopes.agent_body{i}.q(3);
                        obj.polytopes.agent_body{i}.q(4)];

                    plot(Polyhedron(obj.polytopes.agent_body{i}.P,q));
                
                else
                   plot(obj.scenario.ini_state(1,i),obj.scenario.ini_state(3,i),'o','color',obj.colors{i},'MarkerFaceColor', obj.colors{i})
                end
                
            end
%             
            xlabel('rx (m)'); ylabel('ry (m)');
            xticks([x_min:(x_max-x_min)/obj.scenario.grid_size_x:x_max]);
            yticks([y_min:(y_max-y_min)/obj.scenario.grid_size_y:y_max]);
%             
% 
%             axis equal;
            
            
        end
        
        %%%%%%%%%
        function PlotVelocities(obj)
            
                max_n_columns = 3;
                n_columns = min(max_n_columns,obj.mip.number_robots);
                
                
                if obj.mip.number_robots <= max_n_columns 
                    n_rows = 1;
                elseif obj.mip.number_robots <= max_n_columns*2
                    n_rows = 2;
                end
            
            
           

            for j = 1 : obj.mip.number_robots
                 figure(2);  
            subplot(n_rows,n_columns,j)
            plot(obj.vel(1,1:obj.N_opt,j),'k','LineWidth',1.5);
            title("Robot" + j);
            xlabel('Time (s)'); ylabel('v_x');
            
             figure(3);  
             subplot(n_rows,n_columns,j)
            plot(obj.vel(2,1:obj.N_opt,j),'k','LineWidth',1.5);
            title("Robot" + j);
            xlabel('Time (s)'); ylabel('v_y');
            
            end
            
           
            
        end
      
        %%%%%%%%
        function PlotSnapshots(obj)
           
            %figure(); hold on; grid;

            for k = 1 : obj.N_opt
                obj.PlotInMPDGrid(0)
                
                % robots
                for i = 1 : obj.number_agents
                    
                    pos_i = [obj.agents(i).vars.x(1,1:obj.N_opt);obj.agents(i).vars.x(3,1:obj.N_opt)];
                    
                    for j = 1 : k
                        plot(pos_i(1,j),pos_i(2,j),'o','color',obj.colors{i},'MarkerFaceColor', obj.colors{i});
                    end
                    plot(pos_i(1,1:k),pos_i(2,1:k),'LineWidth',2,'color',obj.colors{i});
                    
                    % Plot bodies
                    translated_q = obj.polytopes.agent_body{i}.q + [pos_i(1,k);pos_i(2,k);-pos_i(1,k);-pos_i(2,k)];
                    body_poly = Polyhedron(obj.polytopes.agent_body{i}.P,translated_q);
                    body_poly.plot('Color',obj.colors{i},'alpha',0.8);
                    
                    
                    
                    
                end
                %   print("sim_col_"+k,'-painters','-dpdf')
            end
            
            
        end
        
        %%%%%%%%%
        function PlotCommunicationNetwork(obj,data)
            
            n_col = 3; n_row = 3;
            
            j = 0; figure;
            for k = 1 : data.N_opt
                names = [];
                aux = 0;
                names = ["Base"];
                for i=2:data.number_robots
                    names = [names,"Relay"+(i-1)];
                end
                
                Adj = zeros(data.number_robots);
                for i = 1 : data.number_robots
                    for j = 1: data.number_robots
                        if j~=i
                            Adj(i,j) = data.B_con(data.index.B_con(k,i,j));
                                
                            %Adj(j,i) = obj.mip.opt_vars.B_con(obj.mip.index.B_con(k,i,j));
                        end
                    end
                end
                
                Adj = round(Adj,1);
                
                
                % add base to adjacency matrix
                for i = 1 : data.number_robots
                    aux = [aux, value(data.B_con_base(data.index.base_con(k,i)))];
                    
                end
                Adj = [aux;
                    zeros(data.number_robots,1),Adj];
                
                names = [names,"Leader"];
                
                dig = digraph(Adj,names);
                subplot(n_row,n_col,k)
                plot(dig);
                title("k="+k);
                
                
            end
            
            
        end
        
        %%%%%%%%%
        function PlotOrientations(obj,data)
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;

            for i = 1 : data.number_robots
                 column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                
                 column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            for i = 1 : data.number_robots
                for k = 1 : data.N_opt
                    orientation(k,i) = (pi/12)*(find(round(data.B_orient(data.index.B_orient(k,i,:),1))==1)-1);
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot(k-1,orientation(k,i)*180/pi,'o'); hold on; grid on; 
                    xlim([0 data.N_opt]); ylim([0 360]);
                    xticks([0:1:data.N_opt]);
                    yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Orientation (degrees)');
                    title("Robot "+i)
                    
                end
                hold off;
                
            end
 
        end
        
        %%%%%%%%%
        
        function PlotLinearVelocities(obj)
            
            figure;
            subplot(1,2,1);
            plot([-0.2:0.1:0.2],ones(length([-0.2:0.1:0.2]))*0.22,'k--'); hold on; grid on;
            for j = 1 : obj.mip.number_robots
                for k = 1 : obj.N_opt
                    plot(0,norm(obj.vel(:,k,j)),'rx');
                    
                end
            end
            
            subplot(1,2,2);
            for j = 1 : obj.mip.number_robots
                for k = 1 : obj.N_opt
                    plot(obj.vel(1,k,j),obj.vel(2,k,j),'rx');hold on; grid on;
                    
                end
            end
            plot(obj.polytopeshedron(obj.obj.scenarioario.obj.polytopestopes.vel{1}.P,obj.obj.scenarioario.obj.polytopestopes.vel{1}.q),'alpha',0.0)
            xlim([-0.22 0.22]); ylim([-0.22 0.22]);
            
            
        end
        
        %%%%%%%%%
        
        function PlotAccelerations(obj,data)
            
            max_number_subplot_columns = 3;
            column_coord = 0;
            row_coord = 1;
            
            for i = 1 : data.number_robots
                column_coord = column_coord + 1;
                if column_coord >  max_number_subplot_columns
                    row_coord = row_coord + 1;
                    
                    column_coord= 1;
                end
                plot_coord(:,i) = [row_coord;column_coord];
            end
            
            figure(); 
            
            for i = 1 : data.number_robots
%                    for k = 1 : data.N_opt
%                        lin_vel(k,i) = norm(data.vel(:,k,i));
%                    end
                       
                
                    subplot(size(plot_coord,1),min(data.number_robots,max_number_subplot_columns),i);
                    plot([0:data.N_opt-1],value(data.acc(data.index.lin_vel(1:data.N_opt,i))),'o'); grid;
                   % plot([0:data.N_opt-1],lin_vel(:,i)); grid;
                    xlim([0 data.N_opt]); ylim([-1 1]);
                    xticks([0:1:data.N_opt]);
                    %yticks([0:60:360]);
                    xlabel('Time (s)'); ylabel('Acceleration (m/s^2)');
                    title("Robot "+i)
            end
            
            
            
        end
        
        
        
        
    end % end methods
end % end class

