classdef MIPClass < handle
    properties
        nx = [];
        nu = [];
        number_tar = [];
        number_obs = [];
        number_robots = [];
        number_obs_sides = [];
        number_body_sides = [];
        number_angles = [];
        N = [];
        BigM = [];
        opt_vars = [];
        constraints = [];
        index = [];
        cost = []
        control_weight = [];
        beta = [];
        base = [];
        B_obs_base = [];
        Planner = [];
    end
    
    methods
        function obj = MIPClass(agt,poly)
            % Auxiliary vars
            obj.nx = size(agt(1).dyn.Ad,1);
            obj.nu = size(agt(1).dyn.Bd,2);
            obj.number_robots = length(agt)-1; % number of robots = number of agents - 1
            obj.number_body_sides = size(poly.agent_body{1}.P,1);
            obj.number_tar = length(poly.tar);
            obj.number_obs = length(poly.obs);
            if obj.number_obs > 0
            obj.number_obs_sides = size(poly.obs{1}.P,1);            
            end
            % Optimization parameters
            obj.N = 30;
            obj.BigM = 100;
            obj.control_weight = 0.01;

            % Real-valued optimization variables
            obj.opt_vars.X = sdpvar((obj.N+1)*obj.number_robots*obj.nx,1);
            obj.opt_vars.X_ext = sdpvar((obj.N+1)*2,1); % planar movement of ext agent w/ no vel
            obj.opt_vars.U = sdpvar(obj.N*obj.nu*obj.number_robots,1);
            obj.opt_vars.Abs_U = sdpvar(obj.N*obj.nu*obj.number_robots,1);
            obj.opt_vars.Abs_vel = sdpvar(obj.N*obj.nu*obj.number_robots,1);

            % Binary variables
            obj.opt_vars.B_tar = binvar((obj.N+1)*obj.number_tar*(obj.number_robots+1),1);
            if obj.number_obs > 0; obj.opt_vars.B_obs = binvar((obj.N+1)*obj.number_robots*obj.number_obs*obj.number_obs_sides,1);end
            obj.opt_vars.B_col = binvar((obj.N+1)*sum(1:obj.number_robots)*obj.number_body_sides,1);
            obj.opt_vars.B_con = binvar((obj.N+1)*sum(1:obj.number_robots),1);
            obj.opt_vars.B_hor = binvar(obj.N+1,1);
            
            % Feedback variables
            obj.opt_vars.x_fb = sdpvar(obj.nx*(obj.number_robots),1);
            obj.opt_vars.exp_traj = sdpvar((obj.N+1)*2,1);
            obj.opt_vars.hor_human = intvar(1,1);
            obj.opt_vars.wasTargetVisited = binvar(obj.number_tar-1,1);
        end
        
        function BuildOptVarIndexes(obj)
            
            counter = 0;
            counter_u = 0;
            counter_obs = 0;
            counter_col = 0;
            counter_tar = 0;
            counter_con = 0;
            counter_los = 0;
            counter_ext_agt = 0;

            % External agent state
            for k = 1 : obj.N + 1
                for z = 1 : 2 % rx and ry
                    counter_ext_agt = counter_ext_agt + 1;
                    obj.index.x_ext(z,k) = counter_ext_agt;
                end
            end
                
                
            
            % Remaining 
            for i = 1 : obj.number_robots
                for k = 1 : obj.N+1
                    counter = counter + 1;
                    
                    
                    % robot state
                    obj.index.x(:,k,i) = (counter-1)*obj.nx+1:counter*obj.nx;
                    
                    
                    
                    % obs bin
                    for j = 1 : obj.number_obs
                        for n = 1 : obj.number_obs_sides
                            counter_obs =  counter_obs + 1;
                            obj.index.B_obs(n,k,i,j) = counter_obs; 
                        end
                    end
                    
                    % col bin
                  
                    if i <= obj.number_robots+1
                        for j = i + 1 : obj.number_robots+1
                            for m = 1 : obj.number_body_sides
                                counter_col = counter_col + 1;
                                obj.index.B_col(m,k,i,j) = counter_col;
                            end
                        end
                    end

                    % con bin
                    if i <= obj.number_robots+1
                        for j = i + 1 : obj.number_robots+1
                            counter_con = counter_con + 1;
                            obj.index.B_con(k,i,j) = counter_con;
                        end
                    end
                    
                end

            end
            
             for i = 1 : obj.number_robots
                for k = 1 : obj.N

                     for j = 1 : obj.nu
                         counter_u = counter_u + 1;
                         obj.index.u(j,k,i) = counter_u; % input
                     end
                 end
             end
%             
            
             % target bin
             for i = 1 : obj.number_robots+1 % +1 to consider expert
                 for k = 1 : obj.N+1
                     for j = 1 : obj.number_tar
                         counter_tar = counter_tar + 1;
                         obj.index.B_tar(k,j,i) = counter_tar;
                     end
                 end
             end
             
            
        end
        
        function [constraints, cost, options] = BuildOptProblem(obj,agent,polytopes)
            
            % Constraints
            timer = tic; obj.BuildLinearDynamicConstraints(agent);
            display("Dynamic const build time: "+toc(timer));
            
            timer = tic;obj.BuildStateAndInputConstraints(polytopes);
            display("Pos and input const build time: "+toc(timer));
            
            timer = tic; obj.BuildTargetConstraints(polytopes);
            display("Target const build time: "+toc(timer));
            
            timer = tic; obj.BuildObstacleAvoidanceConstraints(polytopes);
            display("Obs const build time: "+toc(timer));
            
            timer = tic; obj.BuildCollisionAvoidanceConstraints(polytopes);
            display("Col const build time: "+toc(timer));
            
            % Only necessary in matlab
            timer = tic; obj.UpdateExternalAgentTrajectory()
            display("External agent traj. y const build time: "+toc(timer));
            
            timer = tic; obj.BuildAccelerationAbsoluteValueConstraint()
            display("Abs acceleration const build time: "+toc(timer));
            
            timer = tic; obj.BuildVelocitybsoluteValueConstraint()
            display("Abs Velocity const build time: "+toc(timer));
            
            timer = tic; obj.BuildRecedingHorizonConstraints()
            display("Receding horizon const build time: "+toc(timer));
            
            timer = tic; obj.BuildConnectivityConstraints(polytopes);
            display("Connectivity const build time: "+toc(timer));

            
            constraints = [
                obj.constraints.rec_hor
                obj.constraints.external_agent_trajectory
                obj.constraints.set_horizon % forces horizon considering human trajectory (necessary when not minimizing horizon)
                obj.constraints.dyn
                obj.constraints.position
                obj.constraints.acceleration
                obj.constraints.velocity
                obj.constraints.abs_acc
                obj.constraints.targets
                obj.constraints.target_hor_eq
                obj.constraints.horizon
                obj.constraints.tar_limit
                obj.constraints.no_reward_after_horizon
                obj.constraints.obstacles
                obj.constraints.obstacles_binary
                obj.constraints.obstacles_corner_cutting
                obj.constraints.collision_avoidance
                obj.constraints.collision_binary
                obj.constraints.collision_corner_cutting
                obj.constraints.conn_region
                obj.constraints.deg_sum_conn
                obj.constraints.human_conn
                ];
            
            % Cost MAS
            timer = tic;
            cost = obj.BuildCost();
            tfinal = toc(timer);
            display("Cost build time: " + tfinal);
            
            %options = sdpsettings('solver','gurobi');
            options = sdpsettings('solver','gurobi','verbose',0);
            %options.gurobi.NumericFocus = 1;
            %options.gurobi.TimeLimit = 5;
            %options.gurobi.MIPGap = 1e-16;
            %options.gurobi.MIPGapAbs = 1e-16;
            %options.gurobi.OptimalityTol =  1e-9;
            %options.gurobi.MIPFocus = 3;
            
            
        end
        
        function [constraints, cost, options] = BuildOptProblemToExport(obj,agent,polytopes)
            
            % Constraints
            timer = tic; obj.BuildLinearDynamicConstraints(agent);
            display("Dynamic const build time: "+toc(timer));
            
            timer = tic;obj.BuildStateAndInputConstraints(polytopes);
            display("Pos and input const build time: "+toc(timer));
            
            timer = tic; obj.BuildTargetConstraints(polytopes);
            display("Target const build time: "+toc(timer));
            
            timer = tic; obj.BuildObstacleAvoidanceConstraints(polytopes);
            display("Obs const build time: "+toc(timer));
            
            timer = tic; obj.BuildCollisionAvoidanceConstraints(polytopes);
            display("Col const build time: "+toc(timer));
           
            timer = tic; obj.BuildAccelerationAbsoluteValueConstraint()
            display("Abs acceleration const build time: "+toc(timer));
            
            timer = tic; obj.BuildRecedingHorizonConstraints()
            display("Receding horizon const build time: "+toc(timer));
            
            timer = tic; obj.BuildConnectivityConstraints(polytopes);
            display("Connectivity const build time: "+toc(timer));
            
            constraints = [
                obj.constraints.dyn
                obj.constraints.position
                obj.constraints.acceleration
                obj.constraints.velocity
                obj.constraints.abs_acc
                obj.constraints.targets
                obj.constraints.target_hor_eq
                obj.constraints.horizon
                obj.constraints.tar_limit
                obj.constraints.no_reward_after_horizon
                obj.constraints.obstacles
                obj.constraints.obstacles_binary
                obj.constraints.obstacles_corner_cutting
                obj.constraints.collision_avoidance
                obj.constraints.collision_binary
                obj.constraints.collision_corner_cutting
                obj.constraints.conn_region
                obj.constraints.deg_sum_conn
                obj.constraints.human_conn
                ];
            
            % Cost MAS
            timer = tic;
            cost = obj.BuildCost();
            tfinal = toc(timer);
            display("Cost build time: " + tfinal);
            options = sdpsettings('solver','gurobi');
           
        end
        
        function InitializeOpt(obj,const,cost,solver_opt)
        % This function initalizes the optimizer object
         
        outputs = {obj.opt_vars.X, 
                   obj.opt_vars.X_ext,
                   obj.opt_vars.U,
                   obj.opt_vars.Abs_U,
                   obj.opt_vars.B_tar,
               %    obj.opt_vars.B_obs, 
                   obj.opt_vars.B_col,
                   obj.opt_vars.B_hor, 
                   obj.opt_vars.x_fb,
                   obj.opt_vars.wasTargetVisited,
                   obj.opt_vars.exp_traj};
               
        fb_vars = {obj.opt_vars.x_fb,obj.opt_vars.wasTargetVisited,obj.opt_vars.exp_traj,obj.opt_vars.hor_human};
        obj.Planner = optimizer(const,cost,solver_opt,fb_vars,outputs);
            
        end
        
        function [sol,optTime,isThereSolution] = Optimize(obj,x_fb,wasTargetVisited,exp_traj,hor_human)
        % This function calls the optimizer object to plan trajectories
        
        % Calls optimizer object to solve problem
            opt_start = tic;
            [sol,errorcode,~,~,opt.Planner] = obj.Planner(x_fb,wasTargetVisited,exp_traj,hor_human);
            optTime = toc(opt_start);
            disp(yalmiperror(errorcode) + "in " + optTime + " seconds")

            % This is a flag to see if there is any solutions;
            % if there is a solution, the sum of the B_hor is equal to one
            % this flag is also used to stop the loop
            isThereSolution = sum(sol{3}(2:end));
        end
         
        function BuildRecedingHorizonConstraints(obj)
            
            obj.constraints.rec_hor = [];
            for i = 1 : obj.number_robots
                obj.constraints.rec_hor = [obj.constraints.rec_hor,
                    obj.opt_vars.X(obj.index.x(:,1,i)) == obj.opt_vars.x_fb((i-1)*obj.nx+1:i*obj.nx,1)];
            end
        end
                
        function BuildLinearDynamicConstraints(obj,agt)
            A = agt(1).dyn.Ad;
            B = agt(1).dyn.Bd;
            
            obj.constraints.dyn = [];
            obj.constraints.initial_condition = [];
            
            % Applying dynamic constraints only to robots excluding
            % variables attached to external agent
            for i = 1 : obj.number_robots
                
                % Dynamics
                for k = 1 : obj.N
                    
                    obj.constraints.dyn = [obj.constraints.dyn,
                        obj.opt_vars.X(obj.index.x(:,k+1,i)) <= A*obj.opt_vars.X(obj.index.x(:,k,i))+B*obj.opt_vars.U(obj.index.u(:,k,i))+ones(obj.nx,1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];
                    
                    obj.constraints.dyn = [obj.constraints.dyn,
                        -obj.opt_vars.X(obj.index.x(:,k+1,i)) <= -A*obj.opt_vars.X(obj.index.x(:,k,i))-B*obj.opt_vars.U(obj.index.u(:,k,i))+ones(obj.nx,1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];
                end
            end
        end
      
        function BuildStateAndInputConstraints(obj,poly)
            
            % Position bounds
            rx_max = poly.op_region.q(1);
            ry_max = poly.op_region.q(2);
            rx_min = -poly.op_region.q(3);
            ry_min = -poly.op_region.q(4);

            obj.constraints.acceleration = [];
            obj.constraints.velocity = [];
            obj.constraints.position = [];
            
            for k = 1 : obj.N
                
                % Applying dynamic constraints only to robots excluding
                % variables attached to external agent
                for i = 1 : obj.number_robots 
                    
                    % Var assignments
                    pos = [obj.opt_vars.X(obj.index.x(1,k+1,i)); obj.opt_vars.X(obj.index.x(3,k+1,i))];
                    vel = [obj.opt_vars.X(obj.index.x(2,k+1,i)); obj.opt_vars.X(obj.index.x(4,k+1,i))];
                    acc = obj.opt_vars.U(obj.index.u(:,k,i));

                    
                    % Position constraint
                     obj.constraints.position = [obj.constraints.position,
                        pos <= [rx_max;ry_max] + ones(2,1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM,
                        -pos <= -[rx_min;ry_min] + ones(2,1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];

                    obj.constraints.velocity = [obj.constraints.velocity,
                        poly.vel{i}.P*vel <= poly.vel{i}.q + ones(length(poly.vel{i}.q),1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];

                    obj.constraints.acceleration = [obj.constraints.acceleration,
                        poly.vel{i}.P*acc <= poly.vel{i}.q + ones(length(poly.vel{i}.q),1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];

                end
            end
        end
          
        function BuildCollisionAvoidanceConstraints(obj,poly)
            % This is an alternative way of writing the col avoid. const.
            % The problem is that they are different for collisions between
            % robots and between robots and the human when you add
            % robustness.
            
            % First, collision avoidance only between robots
            obj.constraints.collision_avoidance = [];
            obj.constraints.collision_binary = [];
            obj.constraints.collision_corner_cutting = [];
            for k = 1 : obj.N + 1
                for i = 1 : obj.number_robots-1
                    pos_i = [obj.opt_vars.X(obj.index.x(1,k,i));obj.opt_vars.X(obj.index.x(3,k,i))]; % pos of i-th robot at time step k
                    
                    for j = i + 1 : obj.number_robots
                        B_col = obj.opt_vars.B_col(obj.index.B_col(:,k,i,j)); % appropriate collision binary
                        pos_j = [obj.opt_vars.X(obj.index.x(1,k,j)); obj.opt_vars.X(obj.index.x(3,k,j))]; % pos of j-th robot at time step k
                        pos_diff = pos_i - pos_j; % relative position
                        col_avoid_poly = Polyhedron(poly.agent_body{i}.P,poly.agent_body{i}.q) + Polyhedron(poly.agent_body{j}.P,poly.agent_body{j}.q);
                        
                        % Collision polytope constraint
                        obj.constraints.collision_avoidance = [obj.constraints.collision_avoidance,
                            -col_avoid_poly.A*pos_diff<=-col_avoid_poly.b+(ones(4,1)-B_col)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                        
                        % Inter-step collision avoidance
                        if k <= obj.N
                            % Takes next position if i-th and j-th agents
                            pos_i_next = [obj.opt_vars.X(obj.index.x(1,k+1,i)); obj.opt_vars.X(obj.index.x(3,k+1,i))];
                            pos_j_next = [obj.opt_vars.X(obj.index.x(1,k+1,j));obj.opt_vars.X(obj.index.x(3,k+1,j))];
                            pos_diff_next = pos_i_next - pos_j_next; % Next relative position
                            
                            obj.constraints.collision_corner_cutting = [obj.constraints.collision_corner_cutting,
                                -col_avoid_poly.A*pos_diff_next<=-col_avoid_poly.b+(ones(4,1)-B_col)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                        end
                        
                        % obs binary constraint
                        obj.constraints.collision_binary = [obj.constraints.collision_binary,sum(B_col) >= 1];
                        
                    end
                end
            end

            % Now, the human-robot collision avoidance constraints
            % Here, we try to avoid a square centered at the current
            % position of the human encompassing all potential grid cells
            % that it can reach
            
            poly_human_safety_region = Polyhedron(poly.human_safety_region.P,poly.human_safety_region.q);
            %n_body_robot_human = poly_body_1+poly_human_safety_region;
            
            obj.constraints.human_collision_binary = [];
            % Outside loop 
            for i = 1 : obj.number_robots
                poly_robot_body_i = Polyhedron(poly.agent_body{i}.P,poly.agent_body{i}.q);
                human_robot_collision_poly = poly_human_safety_region + poly_robot_body_i;
                
                % Collision avoidance for initial time step %
                % Used just to compute B_col_0 and subsequent corner-cutting ofinitial time-step
                pos_i_0 = [obj.opt_vars.X(obj.index.x(1,1,i));obj.opt_vars.X(obj.index.x(3,1,i))];
                pos_human_0 = [obj.opt_vars.X_ext(obj.index.x_ext(1,1));obj.opt_vars.X_ext(obj.index.x_ext(2,1))];
                pos_diff_0 = pos_i_0 - pos_human_0;
                B_col_0 = obj.opt_vars.B_col(obj.index.B_col(:,1,i,end)); 
                 obj.constraints.collision_avoidance = [obj.constraints.collision_avoidance,
                        -human_robot_collision_poly.A*pos_diff_0<=-human_robot_collision_poly.b+(ones(4,1)-B_col_0)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                
                % Once B_col_0 is determined, write initial corner-cutting
                % constraint
                pos_i_1 = [obj.opt_vars.X(obj.index.x(1,2,i));obj.opt_vars.X(obj.index.x(3,2,i))];
                pos_diff_1 = pos_i_1 - pos_human_0;
                
                obj.constraints.collision_corner_cutting = [obj.constraints.collision_corner_cutting,
                        -poly_human_safety_region.A*pos_diff_1<=-poly_human_safety_region.b+(ones(4,1)-B_col_0)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
            end
            
            % now k > 1
            for k = 1 : obj.N % not N + 1
                for i = 1 : obj.number_robots
                    poly_robot_body_i = Polyhedron(poly.agent_body{i}.P,poly.agent_body{i}.q);
                    human_robot_collision_poly = poly_human_safety_region + poly_robot_body_i;
                    pos_i = [obj.opt_vars.X(obj.index.x(1,k+1,i));obj.opt_vars.X(obj.index.x(3,k+1,i))]; % pos of i-th robot at time step k+1
                    pos_human = [obj.opt_vars.X_ext(obj.index.x_ext(1,k));obj.opt_vars.X_ext(obj.index.x_ext(2,k))]; % pos of human at time step k
                    pos_diff = pos_i - pos_human; % relative position
                    B_col = obj.opt_vars.B_col(obj.index.B_col(:,k+1,i,end)); % appropriate collision binary (end is the human)
                    
                    % Collision polytope constraint
                   % obj.constraints.collision_avoidance = [obj.constraints.collision_avoidance,
                        %-n_body_robot_human.A*pos_diff<=-n_body_robot_human.b+(ones(4,1)-B_col)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                    obj.constraints.collision_avoidance = [obj.constraints.collision_avoidance,
                        -human_robot_collision_poly.A*pos_diff<=-human_robot_collision_poly.b+(ones(4,1)-B_col)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                    
                    % Inter-step collision avoidance
                    if k > 1 && k <= obj.N-1
                        % Takes next position if i-th and j-th agents
                        pos_i_next = [obj.opt_vars.X(obj.index.x(1,k+2,i)); obj.opt_vars.X(obj.index.x(3,k+2,i))];
                        pos_human_next = [obj.opt_vars.X_ext(obj.index.x_ext(1,k+1));obj.opt_vars.X_ext(obj.index.x_ext(2,k+1))];
                        pos_diff_next = pos_i_next - pos_human_next; % Next relative position
                        
                        %obj.constraints.collision_corner_cutting = [obj.constraints.collision_corner_cutting,
                            %-n_body_robot_human.A*pos_diff_next<=-n_body_robot_human.b+(ones(4,1)-B_col)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                        obj.constraints.collision_corner_cutting = [obj.constraints.collision_corner_cutting,
                            -human_robot_collision_poly.A*pos_diff_next<=-human_robot_collision_poly.b+(ones(4,1)-B_col)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k-1))*obj.BigM];
                    end
                    
                    % obs binary constraint
                    obj.constraints.collision_binary = [obj.constraints.collision_binary,sum(B_col) >= 1];
                end
            end
        end
        
        function BuildTargetConstraints(obj,poly)
            
            obj.constraints.targets = [];
            obj.constraints.target_hor_eq  = [];
            obj.constraints.tar_limit = [];
            obj.constraints.no_reward_after_horizon = [];
            
            % Target entry
            for i = 1 : obj.number_robots+1
                for k = 1 : obj.N+1
                    
                    if i > obj.number_robots % if external agent
                        pos = [obj.opt_vars.X_ext(obj.index.x_ext(1,k));obj.opt_vars.X_ext(obj.index.x_ext(2,k))];
                    else % else robot
                        pos = [obj.opt_vars.X(obj.index.x(1,k,i));obj.opt_vars.X(obj.index.x(3,k,i))];
                    end
                    
                    for t = 1 : obj.number_tar
                        B_tar = obj.opt_vars.B_tar(obj.index.B_tar(k,t,i));
                       obj.constraints.targets = [obj.constraints.targets,
                           poly.tar{t}.P*pos <= poly.tar{t}.q + ones(length(poly.tar{t}.q),1)*(1-B_tar)*obj.BigM];
                    end
                end
            end

            % Target binary
            for k = 1 : obj.N+1
                % Horizon is equal to sum of last target entry binary
%                 obj.constraints.target_hor_eq = [obj.constraints.target_hor_eq,
%                     sum(obj.opt_vars.B_tar(obj.index.B_tar(k,obj.number_tar,:))) - obj.opt_vars.B_hor(k) == 0];

                % In this modification, only the external agent ends the
                % mission
                obj.constraints.target_hor_eq = [obj.constraints.target_hor_eq,
                    sum(obj.opt_vars.B_tar(obj.index.B_tar(k,obj.number_tar,end))) - obj.opt_vars.B_hor(k) == 0];
                
            end
            obj.constraints.set_horizon = [];
            obj.constraints.set_horizon = [obj.constraints.set_horizon,
                     obj.opt_vars.B_hor(obj.opt_vars.hor_human) == 1];
            
            % No collection after horizon
            % for k = 1 : obj.N + 1
            for t = 1 : obj.number_tar
                for i = 1 : obj.number_robots+1
                    
                    obj.constraints.no_reward_after_horizon = [obj.constraints.no_reward_after_horizon,
                        [1:obj.N+1]*(obj.opt_vars.B_tar(obj.index.B_tar(:,t,i)) - obj.opt_vars.B_hor) <= 0];
                end
            end


            for t = 1 : obj.number_tar
                
                for i = 1 : obj.number_robots+1
                    aux(i) = sum(obj.opt_vars.B_tar(obj.index.B_tar(:,t,i)));
                end
                
                if t < obj.number_tar
                    obj.constraints.tar_limit = [obj.constraints.tar_limit, sum(aux) <= 1 - obj.opt_vars.wasTargetVisited(t)];
                else
                    obj.constraints.tar_limit = [obj.constraints.tar_limit, sum(aux) <= 1];
                end
            end
            

            % horizon binary must have exactly one entry equal to one
            obj.constraints.horizon = [sum(obj.opt_vars.B_hor) == 1];
        end
        
        function BuildObstacleAvoidanceConstraints(obj,poly)
            
            obj.constraints.obstacles = [];
            obj.constraints.obstacles_binary = [];
            obj.constraints.obstacles_corner_cutting = [];
            for i = 1 : obj.number_robots
                for k = 1 : obj.N+1
                    
                    % Pos agent i
                    pos = [obj.opt_vars.X(obj.index.x(1,k,i));obj.opt_vars.X(obj.index.x(3,k,i))];

                    for j = 1 : obj.number_obs
                        B_obs = obj.opt_vars.B_obs(obj.index.B_obs(:,k,i,j));
                        
                        % Obs entry constraint
                        obj.constraints.obstacles = [obj.constraints.obstacles,
                            -poly.obs{j}.P*pos <= -poly.obs{j}.q+(ones(4,1)-B_obs)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];
                        
                        % Corner cutting
                        if k <= obj.N
                            pos_next = [obj.opt_vars.X(obj.index.x(1,k+1,i));obj.opt_vars.X(obj.index.x(3,k+1,i))];
                            obj.constraints.obstacles_corner_cutting = [obj.constraints.obstacles_corner_cutting,
                                -poly.obs{j}.P*pos_next <= -poly.obs{j}.q+(ones(4,1)-B_obs)*obj.BigM+ones(4,1)*sum(obj.opt_vars.B_hor(1:k))*obj.BigM];
                        end
                        
                        % obs binary constraint
                        obj.constraints.obstacles_binary = [obj.constraints.obstacles_binary, sum(B_obs) >= 1];
                            
                    end
                end
            end
            
            
        end

        function BuildConnectivityConstraints(obj,poly)
            
            %% Conventional connectivity constraints between robots
             obj.constraints.conn_region = [];
             for k = 1 : obj.N + 1
                 for i = 1 : obj.number_robots-1
                     pos_i = [obj.opt_vars.X(obj.index.x(1,k,i));obj.opt_vars.X(obj.index.x(3,k,i))];
                     
                     for j = i + 1 : obj.number_robots
                         pos_j = [obj.opt_vars.X(obj.index.x(1,k,j));obj.opt_vars.X(obj.index.x(3,k,j))];
                         coord_diff =  pos_i - pos_j;
                         B_con = obj.opt_vars.B_con(obj.index.B_con(k,i,j));
                         
                         % Connectivity polytope region constraints
                         conn_poly = poly.nominal_conn_reg;
                         obj.constraints.conn_region = [obj.constraints.conn_region,
                             conn_poly.P*coord_diff <=  conn_poly.q + ones(length(conn_poly.q),1)*(1-B_con)*obj.BigM +...
                             ones(length(conn_poly.q),1)*obj.BigM*sum(obj.opt_vars.B_hor(1:k))];
                     end
                 end
             end
             
             %% Constraint for human connectivity
             for k = 1 : obj.N 
                 for i = 1 : obj.number_robots
                     pos_i = [obj.opt_vars.X(obj.index.x(1,k+1,i));obj.opt_vars.X(obj.index.x(3,k+1,i))];
                     pos_j = [obj.opt_vars.X_ext(obj.index.x_ext(1,k));obj.opt_vars.X_ext(obj.index.x_ext(2,k))];
                     coord_diff =  pos_i - pos_j;
                     B_con = obj.opt_vars.B_con(obj.index.B_con(k+1,i,end));
                     
                     % Connectivity polytope region constraints
                     conn_poly = poly.robust_conn_reg;
                     obj.constraints.conn_region = [obj.constraints.conn_region,
                         conn_poly.P*coord_diff <=  conn_poly.q + ones(length(conn_poly.q),1)*(1-B_con)*obj.BigM +...
                         ones(length(conn_poly.q),1)*obj.BigM*sum(obj.opt_vars.B_hor(1:k))];
                 end
             end
            %% Other constraints
            
            %Build degrees
            B_con = obj.opt_vars.B_con;
            for k = 1 : obj.N + 1
                deg(1,k) = sum(B_con(obj.index.B_con(k,1,2:end)));
                deg(obj.number_robots+1,k) = sum(B_con(obj.index.B_con(k,1:obj.number_robots,obj.number_robots+1)));

                for i = 2 : obj.number_robots
                    deg(i,k) = sum(B_con(obj.index.B_con(k,i,i+1:end))) + sum(B_con(obj.index.B_con(k,1:i-1,i)));     
                     
                end
            end
            
            % Connectivity sufficient conditions
            obj.constraints.deg_sum_conn = [];
            for k = 1 : obj.N + 1
                for i = 1 : obj.number_robots-1 % defined above
                     for j = i + 1 : obj.number_robots % REMOVE +1, you dont consider the human in this because we dont want him to be a relay
                        B_con = obj.opt_vars.B_con(obj.index.B_con(k,i,j));
                        obj.constraints.deg_sum_conn = [obj.constraints.deg_sum_conn,
                            deg(i,k) + deg(j,k) >= obj.number_robots-B_con*obj.number_robots-sum(obj.opt_vars.B_hor(1:k-1))*obj.number_robots];
                    end
                end
            end

            % Connectivity w/ human
            obj.constraints.human_conn = [];
            for k = 2 : obj.N + 1
                B_con_human = obj.opt_vars.B_con(obj.index.B_con(k,:,end)); 
                obj.constraints.human_conn = [obj.constraints.human_conn,
                    sum(B_con_human) >=1];
            end

        end
        
        function BuildAccelerationAbsoluteValueConstraint(obj)
           obj.constraints.abs_acc = [-obj.opt_vars.Abs_U  <= obj.opt_vars.U <= obj.opt_vars.Abs_U]; 
        end
        
        function BuildVelocitybsoluteValueConstraint(obj)
            
            obj.constraints.abs_vel = [];
            vel = [];
            for k = 1 : obj.N
                for i = 1 : obj.number_robots 
                    vel = [vel;obj.opt_vars.X(obj.index.x(2,k+1,i)); obj.opt_vars.X(obj.index.x(4,k+1,i))];
                    %obj.constraints.abs_vel = [obj.constraints.abs_vel,
                    %    -obj.opt_vars.Abs_vel  <= vel <= obj.opt_vars.Abs_vel]; 
                end
            end
            obj.constraints.abs_vel = [-obj.opt_vars.Abs_vel  <= vel <= obj.opt_vars.Abs_vel]; 
        end
              
        function cont_traj = Discrete2ContinuousTraj(obj,grid_traj,scenario)
            
            half_c_size_x = scenario.cell_size_x/2;
            half_c_size_y = scenario.cell_size_y/2;
            half_g_size_x = scenario.op_region_size_x/2;
            half_g_size_y = scenario.op_region_size_y/2;
            
            for i = 1 : length(grid_traj)
                aux = [];
                for k = 1 : length(grid_traj{i})
                    grid_x = grid_traj{i}{k}(1);
                    grid_y = grid_traj{i}{k}(2);
                    
                    cont_x = -half_g_size_x + 2*grid_x*half_c_size_x  + half_c_size_x; % cont_x = -3 + grid_x*scenario.cell_size_x/2 + scenario.cell_size_x/2,
                    cont_y =  half_g_size_y - 2*grid_y*half_c_size_y  - half_c_size_y;
                    aux = [aux; [cont_x;cont_y] ]; 
                    
                end
                cont_traj{i} = aux;
                lines2add = length(obj.opt_vars.exp_traj) - length(cont_traj{1}); % Fix size for feedback
                cont_traj{i} = [cont_traj{i};repmat(cont_traj{1}(end-1:end),[lines2add/2,1])];
            end
        end
        
        function UpdateExternalAgentTrajectory(obj)
            
           % N_traj = length(traj_ext);
            obj.constraints.external_agent_trajectory = [];
            for k = 1 : obj.N + 1
                
                    ind = [(k-1)*2+1,2*k];
                    obj.constraints.external_agent_trajectory = [obj.constraints.external_agent_trajectory,
                        obj.opt_vars.X_ext(obj.index.x_ext(1,k)) == obj.opt_vars.exp_traj(ind(1)),
                        obj.opt_vars.X_ext(obj.index.x_ext(2,k)) == obj.opt_vars.exp_traj(ind(2)),
                        ];
                
            end
        end
        
        function cost = BuildCost(obj)
            
            
            rho = 5*ones(obj.number_tar-1,1);
            aux = 0;
            for i = 1 : obj.number_robots+1
                %if i > obj.number_robots
                   % rho = rho*5;
                %end
                for t = 1 : obj.number_tar-1
                    aux = aux + rho(t)*sum(obj.opt_vars.B_tar(obj.index.B_tar(:,t,i)));
                end
            end
            
            %cost = [1:obj.N+1]*obj.opt_vars.B_hor + obj.control_weight*sum(obj.opt_vars.Abs_U) - aux;
            cost = obj.control_weight*sum(obj.opt_vars.Abs_U) - aux;
            %cost = obj.control_weight*sum(obj.opt_vars.Abs_vel) - aux;
            
            
        end
        
    end % end methods
end % end class

