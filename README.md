# ARMCHAIR

## Dependencies

1. See Markus Peschl's [MORAL](https://github.com/mlpeschl/moral_rl) for required RL and IRL stuff
1. The MPC and MIP open loop schemes require a model in the MPS (*Mathematical Programming System*) format
1. If you use the code provided to build your model in MATLAB, the [Yalmip](https://yalmip.github.io/)  package is necessary
1. You will need a solver for the MPS model. ARMCHAIR employs a Mixed-Integer Programming Model and the [Gurobi] solver under an academic license. Item 2. and 3. are not needed if you are interested only in the Reinforcement Learning and Inverse Reinforcement Learning elements.
1. Python packages used when continuing development of the AIRL model can be installed with ```pip install -r /path/to/airl/requirements.txt```

## Comments

- The optimization models used in this code are not built in python, but in MATLAB using the yalmip package. They are exported from MATLAB in the *.mds* format and loaded and solved in the python code. This is done since yalmip is a very convenient way of writing complex optimization mixed-integer programming models
- Make sure you unpack ./airl/demos/*.zip if you want to replicate the respective results
- Make sure you unpack ./python_mpc/data_results_mip_and_mpc.zip if you wnat to replicate results
- The snapshots of the 6000 simulations performed are avaliable [here](https://drive.google.com/file/d/1hGC2G6W5H0DTipIKyN-GIZ7QRRq3pFW_/view?usp=sharing)
- Results obtained when continuing development of the AIRl model are available [here]()

## Files

- AIRL Folder
    - **airl.py** - Adversial Inverse Reinforcement algoritm
    - **airl_train_plot.py** - Used to train AIRL given a set of demonstrations and hyperparameters, also used to obtain GIF results of policies.
    - **airl_HP_train.py** - Used to tune the hyperparameters of AIRL, number of trials and HP ranges can be adjusted.
    - **eval_policy_comparison.py** - Use to generate data to compare policies
    - **eval_policy_montecarlo.py** - Use to generate data for table 3 (manuscript)
    - **generate_demos.py** - Create dataset of demonstrations given an expert (synthetic human) policy
    - **plot_montecarlo_results.py** - Plots results from eval_policy_montecarlo.py
    - **ppo.py** - Proximal Policy Optimization algorithm
    - **ppo_train.py** - Used to train the synthetic human policy
    - **profiling_airl_train.py** - Used to complete python profiling on the training

- MATLAB_MPC Folder
    - **AgentClass.m** - Agent class
    - **AnalysisClass.m** - Analysis class
    - **export_grb_model.m** - This script builds and exports the MIP model (.mds file) used in the python code (requires MATLAB's yalmip package)
    - **main_MPC_MAS.m** - This script runs a simulation using the above model (requires a MIP solver)
    - **PolytopeClass.m** - Polytope class
    - **ScenarioClass.m** - Scenario class
    
- python_MPC Folder
    - **analysis.py** - Analysis class
    - **external_agent.py** - Synthetic human class
    - **main_MIP.py** - Run simulations with the MIP open-loop scheme
    - **main_MPC.py** - Run simulations with the ARMCHAIR (MPC) scheme
    - **main_res_analysis.py** - Statistical analysis of monte carlo simulations
    - **motion_planner.py** - Motion planner class
    - **multirobot_system.py** - Multirobot class
    - **parameters.py** - File with robot/human specifications
    - **ppo.py** - Required to load expert policy


