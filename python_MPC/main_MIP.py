
from external_agent import *
from analysis import *
from motion_planner import *
from multirobot_system import *
import parameters
import pickle

env_id = 'Environment1-v0' 
# env_id = 'SmallMuseumCollect-v0' # make sure its registered at ~/.local/lib/python3.8/site-packages/gym/envs
#ext_agt_model_id = './pytorch_models/less_mov_penalty.pt' # Model of the external agent
ext_agt_model_id = './pytorch_models/less_mov_penality_6hours.pt'

#prediction_model_id = './pytorch_models/sleek-dust-253.pt' # Prediction model from AIRL
prediction_model_id = './pytorch_models/generator_less_mov_penality_6hours.pt' 

#mpc_model_id = "./gurobi_models/MPCModel_environment1_new.mps"
mpc_model_id = "./gurobi_models/MPCModel_environment2_new.mps"

#env_params, mrs_params, matlab_params = parameters.get_parameters('environment1')
env_params, mrs_params, matlab_params = parameters.get_parameters('environment2')

N = 35
number_of_experiments = 1000
data = [] # data vector of results for each experiment

# SIMULATION LOOP
for n in range(number_of_experiments):
    print('Experiment number: ',n)
    #print(' ')
    # Reset environment
    # set env_params to none if you want to randomize environment!!!
    #env_params = None
    ext_agt = ExternalAgent(env_id,ext_agt_model_id,env_params)
    mrs = MultiRobotSystem(mrs_params)
    x_fb = mrs.initial_state
    states = ext_agt.ini_states
    states_tensor = torch.tensor(ext_agt.ini_states).float().to(device)

    actions = [[]]

    # MIP operates in open-loop so:
    # a) we predict the human once
    # b) we solve the optimization once

    # Predict external agent trajectory
    #ext_agt_pred_traj = motion_planner.predict_external_agent(states,0) # USE THIS IF OPEN-LOOP FROM MODEL
   # ext_agt_openLoop_traj = ext_agt_pred_traj
    if n == 0:
        motion_planner = MotionPlanner(env_id,prediction_model_id,mpc_model_id,env_params,matlab_params,ext_agt)

        """
        # Perfect predictction
        ext_agt.compute_real_intended_traj(states) # USE THIS IF OPEN-LOOP FROM HUMAN
        ext_agt_openLoop_traj = ext_agt.intended_trajectories[0]
        motion_planner.predicted_trajectories.append(ext_agt_openLoop_traj)
        """ 
        # AIRL model prediction (alternative to perfect open-loop prediction)
        ext_agt_openLoop_traj = motion_planner.predict_external_agent(states,0) # USE THIS IF OPEN-LOOP FROM MODEL

        # Update feedback variables (done once since open-loop)
        #B_col_0 = motion_planner.compute_human_collision_binary(ext_agt_openLoop_traj,x_fb)   
        motion_planner.update_mpc_feedback(ext_agt_openLoop_traj,x_fb,np.zeros([len(motion_planner.q),1],dtype=bool))

        # Optimize MIP
        u_k, u_seq, mrs_pred_trajs_k,flag = motion_planner.optimize()
        #mrs.save_data(x_fb,u_k,mrs_pred_trajs_k)

        # Targets visited by robots can be computed a priori since there is no update
        # here

        #  If unfesiable stops b4 simulation
        if flag == "Unfeasible":
            print('Unfeasible optimization problem')
            exit()

    motion_planner.WTV_by_agent = np.zeros([matlab_params['n_agents'],4])
    WTV = np.zeros([len(motion_planner.q),1],dtype=bool) # Was Target Visited? Boolean vector
    u_k = np.zeros([matlab_params['nu'],matlab_params['n_agents']-1])
    for k in range(N):
        
        #print('Time step: ', k )
        collectFlag = False # reset

        # Saves discrete and continuous positions at this time step
        ext_agt.save_position(states)

        # Computes real intended trajectory of external agent for plots and analysis (comparing w/ predicted traj)
        ext_agt.compute_real_intended_traj(states) # the traj is saved within the class

        if actions[0] == 4:
            collectFlag = True

        WTV = motion_planner.check_target_visitation_open_loop(ext_agt.cont_traj[k],x_fb,collectFlag,k)

        states = ext_agt.update_feature_matrix(WTV,states)
        states_tensor = torch.tensor(states).float().to(device)

        # MPC-MIP multi-robot system
        for i in range(matlab_params['n_agents']-1):
            if k >= len(u_seq[1,:,i]): # if the human operates after the horizon, stop robots
                u_k[0,i] = -2*x_fb[1,i]
                u_k[1,i] = -2*x_fb[3,i]
            else: #otherwise, use optimal control sequence
                u_k[:,i] = u_seq[:,k,i]
        mrs.save_data(x_fb,u_k,mrs_pred_trajs_k)

        # Stops forwarding dynamics after optimal horizon passes (agents stop at final location)
        if k <= motion_planner.opt_hor-2:
            x_fb = mrs.forward_dynamics(x_fb,u_k)
        else:
           x_fb = x_fb

        #if k > 0: # no reason to check first step
           #WTV = motion_planner.check_target_visitation_open_loop(ext_agt.cont_traj[k],x_fb,WTV,collectFlag,k+1)
            #if k >= 12:
                #stop = 1

            #WTV = motion_planner.check_target_visitation_open_loop(ext_agt.cont_traj[k],x_fb,collectFlag,k+1)
        # External agent dynamics
        actions, log_probs = ext_agt.ppo.act(states_tensor)
        next_states, rewards, done , _ = ext_agt.env.step(actions[0]) # done is related to 'k' (states) not 'k+1' (next_states)
        states = next_states.copy()

        #print('Targets visited at time step ', k+1, ': ', np.array(WTV).transpose())
        #print(' ')

        if done or WTV[-1]:
            # save data from n-th experiment
            # The data are the trajectories from 1) robot, 2) human, predicted ones, and targets visited by each agent
            data.append([mrs.robot,ext_agt.cont_traj, motion_planner.predicted_trajectories,motion_planner.WTV_by_agent])
            #print('agent2, experiment :', n, ' ',motion_planner.WTV_by_agent[2,:])
            break

        

# Analysis --------------------------------------------

analysis = Analysis(ext_agt,mrs,motion_planner)
analysis.export_data_for_analysis(data,filename='mip_data_env2')

if flag != "Unfeasible":
    n_collisions = analysis.check_collisions()

# Plots
save_snapshots = False
plot_snapshots = False
isClosedLoop = False
plot_predictions = True
print('agent0: ',motion_planner.WTV_by_agent[0,:])
print('agent1: ',motion_planner.WTV_by_agent[1,:])
print('agent2: ',motion_planner.WTV_by_agent[2,:])
analysis.plot_snapshots(save_snapshots,plot_snapshots,0,isClosedLoop,plot_predictions)
#analysis.plot_only_external_agent_snapshots(save_snapshots,plot_snapshots)


