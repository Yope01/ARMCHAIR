
from external_agent import *
from analysis import *
from motion_planner import *
from multirobot_system import *
import parameters
import pickle

# although its called environment-1 it is used for environment 1 and 2. 
# The difference in envs 1 and 2 are just the target and agent placements, which are done via parameter.py
# To change obstacles and terminal region, see 'Environment2-v0'
env_id = 'Environment1-v0' 
#ext_agt_model_id = './pytorch_models/less_mov_penalty.pt' # Model of the external agent
ext_agt_model_id = './pytorch_models/less_mov_penality_6hours.pt'

#prediction_model_id = './pytorch_models/sleek-dust-253.pt' # Prediction model from AIRL
prediction_model_id = './pytorch_models/generator_less_mov_penality_6hours.pt' 

# MPC model from matlab
#mpc_model_id = "./gurobi_models/MPCModel_environment1_new.mps"
mpc_model_id = "./gurobi_models/MPCModel_environment2_new.mps"

#env_params, mrs_params, matlab_params = parameters.get_parameters('environment1')
env_params, mrs_params, matlab_params = parameters.get_parameters('environment2')

N = 35
number_of_experiments = 1000
data = [] # data vector of results for each experiment

# SIMULATION LOOP
for n in range(number_of_experiments):
    print('Experiment: ', n)

    # Reset environment
    # set env_params to none if you want to randomize environment!!!
    #env_params = None
    ext_agt = ExternalAgent(env_id,ext_agt_model_id,env_params)
    motion_planner = MotionPlanner(env_id,prediction_model_id,mpc_model_id,env_params,matlab_params,ext_agt)
    mrs = MultiRobotSystem(mrs_params)
    x_fb = mrs.initial_state
    WTV = np.zeros([len(motion_planner.q),1],dtype=bool) # Was Target Visited? Boolean vector

    states = ext_agt.ini_states
    states_tensor = torch.tensor(ext_agt.ini_states).float().to(device)

    actions = [[]]

    for k in range(N):
        
        print('Time step: ', k , ' ------- Experiment: ', n)
        collectFlag = False # reset

        # Saves discrete and continuous positions at this time step
        ext_agt.save_position(states)

        # Computes real intended trajectory of external agent for plots and analysis (comparing w/ predicted traj)
        ext_agt.compute_real_intended_traj(states) # the traj is saved within the class

        # Predict external agent trajectory
        ext_agt_pred_traj = motion_planner.predict_external_agent(states,k) # REMOVE K IT IS ONLY USED FOR TESTS

        # Verifies which targets where visited to update the visitation flag variable
        # that is then used as feedback for the MPC, preventing agents from visiting targets already visited
        if actions[0] == 4:
            collectFlag = True
        
        if k > 0: # no reason to check first step
            WTV = motion_planner.check_target_visitation(ext_agt_pred_traj[0],x_fb,WTV,collectFlag)            \
        
        # Also update the feature matrix of the targets if a robot captures a target (this is how the human reacts to the robots)
        states = ext_agt.update_feature_matrix(WTV,states)
        states_tensor = torch.tensor(states).float().to(device)

        # Compute human collision binary for x_k
        #B_col_0 = motion_planner.compute_human_collision_binary(ext_agt_pred_traj,x_fb)     
        
        # Update feedback variables
        motion_planner.update_mpc_feedback(ext_agt_pred_traj,x_fb,WTV)
    
        # Optimize MIP
        u_k,u_seq, mrs_pred_trajs_k,flag = motion_planner.optimize()

        # MPC-MIP multi-robot system
        mrs.save_data(x_fb,u_k,mrs_pred_trajs_k)
        x_fb = mrs.forward_dynamics(x_fb,u_k)

        # External agent dynamics
        actions, log_probs = ext_agt.ppo.act(states_tensor)
        next_states, rewards, done , _ = ext_agt.env.step(actions[0]) # done is related to 'k' (states) not 'k+1' (next_states)
        print(rewards)
        states = next_states.copy()

        #  Termination conditions
        if flag == "Unfeasible":
            print('Unfeasible optimization problem')
            print('Stopping...')
            #break

        print('Targets visited at time step ', k, ': ', np.array(WTV).transpose())
        print(' ')

        
        if done or WTV[-1]:
            # save data from n-th experiment
            # The data are the trajectories from 1) robot, 2) human, predicted ones, and targets visited by each agent
            data.append([mrs.robot,ext_agt.cont_traj, motion_planner.predicted_trajectories,motion_planner.WTV_by_agent])
            break

# Analysis --------------------------------------------

analysis = Analysis(ext_agt,mrs,motion_planner)
#analysis.export_data_for_analysis(data,filename='mpc_irl_data')
#analysis.export_data_for_analysis(data,filename='mpc_irl_data_env2')
if flag != "Unfeasible":
    n_collisions = analysis.check_collisions()

# Plots
save_snapshots = False
plot_snapshots = False
plot_predictions = False
isClosedLoop = True
print('agent0: ',motion_planner.WTV_by_agent[0,:])
print('agent1: ',motion_planner.WTV_by_agent[1,:])
print('agent2: ',motion_planner.WTV_by_agent[2,:])
analysis.plot_snapshots(save_snapshots,plot_snapshots,0,isClosedLoop,plot_predictions)
#analysis.plot_only_external_agent_snapshots(save_snapshots,plot_snapshots)


