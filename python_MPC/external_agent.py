
from ppo import *
import gym
import copy


class ExternalAgent:

    def __init__(self,env_id,ext_agt_model_id,env_params):
        
        width = env_params['env_width']
        length = env_params['env_length']
        self.human_width = env_params['human_width']
        self.human_length = env_params['human_length']
        self.env_params = env_params

        self.env = gym.make(env_id)
        self.env.reset()


        #if env_params == None: # randomize scenario
        if env_params['tar_list_A'] == None and env_params['tar_list_B'] == None: # randomize scenario
            self.ini_states = self.env.reset()
        else:
            self.ini_states = self.env.set(env_params)

        self.disc_traj = []
        self.cont_traj = []
        self.intended_trajectories = []

        # Fetch Shapes
        n_actions = self.env.action_space.n
        obs_shape = self.env.observation_space.shape
        state_shape = obs_shape[1:]
        in_channels = obs_shape[0]

        self.ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
        self.ppo.load_state_dict(torch.load(ext_agt_model_id))

        # Continuous scenario parameters
        #width = 7.0
        #length = 7.0
        x_cell_size = width/self.env.size_hor 
        y_cell_size = length/self.env.size_ver

        self.param_cont_scen = {
            "Width" : width,
            "Length" : length,
            "x_cell_size" : x_cell_size, 
            "y_cell_size" : y_cell_size
        }

    def update_feature_matrix(self,WTV,states):
        WTV_A = WTV[-(len(self.env_params['tar_list_A'])+1):-1]
        WTV_B = WTV[0:len(self.env_params['tar_list_B'])]

        for b in range(len(WTV_B)):
            if WTV_B[b]: # if there is a target there
                states[2][self.env_params['tar_list_B'][b]] = 0.0 #removes it if visited
        for a in range(len(WTV_A)):
            if WTV_A[a]:
                states[1][self.env_params['tar_list_A'][a]] = 0.0

        return states

    def save_position(self,states):
        # Save positions for plots afterwards
        y = np.where(states[0]==1)[0][0] # row
        x = np.where(states[0]==1)[1][0] # col 
        self.disc_traj.append([y,x])

        cont_coord = self.discrete_to_continuous_coord([y,x])
        self.cont_traj.append(cont_coord)

    def discrete_to_continuous_coord(self,coord):
        half_w = self.param_cont_scen["Width"]/2
        half_l = self.param_cont_scen["Length"]/2
        half_c_size_x = self.param_cont_scen["x_cell_size"]/2
        half_c_size_y = self.param_cont_scen["y_cell_size"] /2

        # Conversion to continuous[y,x]
        cont_coord = [ half_l - 2*coord[0]*half_c_size_y - half_c_size_y , -half_w + 2*coord[1]*half_c_size_x + half_c_size_x ]

        return cont_coord


    def compute_real_intended_traj(self,ini_states):

        # Creates a local environment every time, to prevent any conflict with the real environment variables

        intended_cont_traj = []
        env_local = copy.deepcopy(self.env)
        local_states = ini_states.copy()
        local_states_tensor = torch.tensor(local_states).float().to(device)
        intended_cont_traj = []
        for k in range(30):

            y = np.where(local_states[0]==1)[0][0] # row
            x = np.where(local_states[0]==1)[1][0] # col 

            cont_coord = self.discrete_to_continuous_coord([y,x])
            intended_cont_traj.append(cont_coord)

            actions, log_probs = self.ppo.act_deterministic(local_states_tensor) # uses same policy
            local_next_states, rewards, done , _ = env_local.step(actions) 
            local_states = local_next_states.copy()
            local_states_tensor = torch.tensor(local_states).float().to(device)

            #if done:
                #break
        
        self.intended_trajectories.append(intended_cont_traj)

            