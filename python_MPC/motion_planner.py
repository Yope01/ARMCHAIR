
from ppo import *
import gym
import gurobipy as gp
from gurobipy import GRB

class MotionPlanner:

    def __init__(self,env_id,prediction_model_id,mpc_model_id,env_params,matlab_params,ext_agt):

        ####
        #### Initialize gurobi
        self.mpc_model = gp.read(mpc_model_id)
        self.mpc_model.params.LogToConsole = 0
        self.mpc_model.params.MIPGap = 0.000001
        self.sol_vec = [] # vector full solutions (X) for each time step
        self.params_from_matlab = matlab_params
        self.params_from_env = env_params

        ####
        #### Stuff for target visitation checking
        half_w = ext_agt.param_cont_scen["Width"]/2
        half_l = ext_agt.param_cont_scen["Length"]/2
        half_c_size_x = ext_agt.param_cont_scen["x_cell_size"]/2
        half_c_size_y = ext_agt.param_cont_scen["y_cell_size"] /2
        eps = 0.001 # there is this margin in matlab

        tar_A_cells = ext_agt.ini_states[1].copy()
        tar_B_cells = ext_agt.ini_states[2].copy()
        tar_A_coords =np.stack(np.where(tar_A_cells==1),axis=0)
        tar_B_coords =np.stack(np.where(tar_B_cells==1),axis=0)
        self.terminal_location = ext_agt.env.terminal_location
        
        # for halfspace representation used to check target visitation (see target visitation method)
        self.P = np.matrix([[1.0,0.0],[0.0,1.0],[-1.0,0.0],[0.0,-1.0]]) 
        self.q = []  

        # TARGET ORDER IS IMPORTANT HERE - MAKE SURE IS EQUAL TO MATLAB
        # Target A
        for i in range(len(tar_A_coords[0])):
           c = ext_agt.discrete_to_continuous_coord(tar_A_coords[:,i])
           self.q.append( np.array([c[1] + half_c_size_x, c[0] + half_c_size_y, -(c[1]-half_c_size_x), -(c[0]-half_c_size_y)]) )
        
        # Target B
        for i in range(len(tar_B_coords[0])):
           c = ext_agt.discrete_to_continuous_coord(tar_B_coords[:,i])
           self.q.append( np.array([c[1] + half_c_size_x, c[0] + half_c_size_y, -(c[1]-half_c_size_x), -(c[0]-half_c_size_y)]) )

        # Terminal cell
        c = ext_agt.discrete_to_continuous_coord(self.terminal_location)
        self.q.append( np.array([c[1] + half_c_size_x, c[0] + half_c_size_y, -(c[1]-half_c_size_x), -(c[0]-half_c_size_y)]))

        # REORDERING OF TARGETS: ATTENTION!! THE ORDER OF TARGETS IN MATLAB IS DIFFERENT THAN HERE
        # THE CORRECTION IS DONE MANUALLY HERE. TODO: AUTOMATE SOMEHOW
        aux = self.q.copy()
        
        if mpc_model_id == './gurobi_models/MPCModel_environment1_new.mps':
            self.q[0] = aux[3]
            self.q[1] = aux[2]
            self.q[2] = aux[1]
            self.q[3] = aux[0]
        elif mpc_model_id == './gurobi_models/MPCModel_environment2_new.mps':
            self.q[2] = aux[3]
            self.q[3] = aux[2]

        """
        self.q[0] = aux[0]
        self.q[1] = aux[2]
        self.q[2] = aux[3]
        self.q[3] = aux[1]
        """
        self.n_targets = len(self.q)

        # add margins (eps=0.001) to avoid numerical erros
        for z in range(len(self.q)):
            self.q[z] = self.q[z] + eps 

        ####
        #### Initialize gym env for ext agent predicitons
        self.env = gym.make(env_id)
        self.env.reset()

        if env_params == None: # randomize scenario
            self.ini_states = self.env.reset()
        else:
            self.ini_states = self.env.set(env_params)

        self.disc_traj = []
        self.cont_traj = []

        gamma = 0.999

        # Fetch Shapes
        n_actions = self.env.action_space.n
        obs_shape = self.env.observation_space.shape
        state_shape = obs_shape[1:]
        in_channels = obs_shape[0]

        self.ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
        self.ppo.load_state_dict(torch.load(prediction_model_id))

        # List to save predicted trajectories of external agent at each time step
        self.predicted_trajectories = []

        # Parameters of the continuous environment
        self.param_cont_scen = ext_agt.param_cont_scen

        # This is used to verify if there were redundant target collections (it excludes the terminal)
        self.WTV_by_agent = np.zeros([self.params_from_matlab['n_agents'],self.n_targets-1])

        self.last_hor = 0

    def predict_external_agent(self,states,k):
        # Predicts future continuous trajectory
        # Also interpolates to generate positions
        # between cell points
        N = 30
        states_tensor = torch.tensor(states).float().to(device)

        # update env internal initial state
        

        # Resets prediction vector
        self.cont_traj = []
        self.env.reset()
        self.env.update_state(states)
        # Prediction

        for k in range(N):

            # Save continuous state
            y = np.where(states[0]==1)[0][0] # row
            x = np.where(states[0]==1)[1][0] # col 

            cont_coord = self.discrete_to_continuous_coord([y,x])
            self.cont_traj.append(cont_coord)

            # Next state
            actions, log_probs = self.ppo.act_deterministic(states_tensor)
            next_states, rewards, done , _ = self.env.step(actions) # done is related to 'k' (states) not 'k+1' (next_states)
   
            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)

            if done:
                break

        # Completes the position vector with last state to complete it
        size_current_traj = len(self.cont_traj)

        for i in range(N-size_current_traj):
            #self.cont_traj.append(self.cont_traj[size_current_traj-1])
             self.cont_traj.append([2.0,2.0])

        self.predicted_trajectories.append(self.cont_traj)
        return self.cont_traj

    def check_target_visitation(self,x_ext,x_robots,WTV,collectFlag):

        N = self.params_from_matlab['N']
        nx = self.params_from_matlab['nx']
        n_agents = self.params_from_matlab['n_agents']
        ny = self.params_from_matlab['ny']
        nu = self.params_from_matlab['nu']

        # To check if a robot is really collecting a target, me must:
        #
        # a) check if the robot is within the boundaries of a target now
        # b) see if its "intention" was to collect: b_tar(k | k-1 )=1
        #
        # step 'b' requires that we check the last solution in sol_vec[-1]
        #

        # This offset is used  to recover b_tar from the full vector of solutions X
        b_tar_offset = (N+1)*nx*(n_agents-1) + ny*(N+1) + 2*N*nu*(n_agents-1)
        
        #n_targets = len(self.q)
        n_agents = self.params_from_matlab['n_agents']

        for i in range(n_agents):

            # Targets
            if i < n_agents-1: # if robots
                x_i = np.matrix([x_robots[0,i],x_robots[2,i]]) # select correct state
                for j in range(self.n_targets-1): # for each target except terminal one
                    index = b_tar_offset + (i*(N+1)*self.n_targets + self.n_targets + j) #index of correct b_tar (I hope)
                    b_tar = self.sol_vec[-1][index]
                    
                    #if not WTV[j][0] and b_tar: # if not already visited 
                    if  b_tar: 
                        #WTV[j][0] = all(self.P*x_i.T <= np.matrix(self.q[j]).T)
                        self.WTV_by_agent[i,j] = all(self.P*x_i.T <= np.matrix(self.q[j]).T)
                         

            else: # if human
                x_i = np.matrix([x_ext[1],x_ext[0]])
                for j in range(self.n_targets-1): # for each target
                    #if not WTV[j][0] and collectFlag: # if not already visited and collect action
                    if not self.WTV_by_agent[i,j] and collectFlag:
                        #WTV[j][0] = all(self.P*x_i.T <= np.matrix(self.q[j]).T) 
                        self.WTV_by_agent[i,j] = all(self.P*x_i.T <= np.matrix(self.q[j]).T) 

            # Terminal state (only external agent)
            #if not WTV[-1][0]: # collection is not necessary
              # WTV[-1][0] = all(self.P*np.matrix([x_ext[1],x_ext[0]]).T <= np.matrix(self.q[-1]).T) 
            wasLastTargetVisited = all(self.P*np.matrix([x_ext[1],x_ext[0]]).T <= np.matrix(self.q[-1]).T) 

        # Determine WTV based on WTV by agent 
        WTV = np.concatenate((np.array(np.sum(self.WTV_by_agent, axis = 0),dtype=bool) , [wasLastTargetVisited]))
                
        return WTV
        
    def check_target_visitation_open_loop(self,x_ext,x_robots,collectFlag,k):
        # this is the same as check_target_visitation, but since its with open loop planning
        # the treatment of b_tar is different

        N = self.params_from_matlab['N']
        nx = self.params_from_matlab['nx']
        n_agents = self.params_from_matlab['n_agents']
        ny = self.params_from_matlab['ny']
        nu = self.params_from_matlab['nu']

        # To check if a robot is really collecting a target, me must:
        #
        # a) check if the robot is within the boundaries of a target now
        # b) see if its "intention" was to collect: b_tar(k |0)=1
        
        # This offset is used  to recover b_tar from the full vector of solutions X
        b_tar_offset = (N+1)*nx*(n_agents-1) + ny*(N+1) + 2*N*nu*(n_agents-1)

        #n_targets = len(self.q)
        n_agents = self.params_from_matlab['n_agents']

        for i in range(n_agents):

            # Targets
            if i < n_agents-1: # if robots
                x_i = np.matrix([x_robots[0,i],x_robots[2,i]]) # select correct state
                for j in range(self.n_targets-1): # for each target except terminal one
                    index = b_tar_offset + (i*(N+1)*self.n_targets + k*self.n_targets + j) #index of correct b_tar (I hope)
                    b_tar = self.sol_vec[0][index]
                    
                    #if not WTV[j][0] and b_tar: # if not already visited 
                    if  b_tar: # remove not WTV to register redundant visits
                        #WTV[j][0] = all(self.P*x_i.T <= np.matrix(self.q[j]).T)
                        self.WTV_by_agent[i,j] = all(self.P*x_i.T <= np.matrix(self.q[j]).T)
       
            else: # if human
                x_i = np.matrix([x_ext[1],x_ext[0]])
                for j in range(self.n_targets-1): # for each target
                   if not self.WTV_by_agent[i,j]  and collectFlag: # if not already visited and collect action
                        #WTV[j][0] = all(self.P*x_i.T <= np.matrix(self.q[j]).T) 
                        self.WTV_by_agent[i,j] = all(self.P*x_i.T <= np.matrix(self.q[j]).T) 

            # Terminal state (only external agent)
            #if not WTV[-1][0]: # collection is not necessary
            wasLastTargetVisited = all(self.P*np.matrix([x_ext[1],x_ext[0]]).T <= np.matrix(self.q[-1]).T) 

        # Determine WTV based on WTV by agent 
        WTV = np.concatenate((np.array(np.sum(self.WTV_by_agent, axis = 0),dtype=bool) , [wasLastTargetVisited]))
        
                
        return WTV

    def discrete_to_continuous_coord(self,coord):

        half_w = self.param_cont_scen["Width"]/2
        half_l = self.param_cont_scen["Length"]/2
        half_c_size_x = self.param_cont_scen["x_cell_size"]/2
        half_c_size_y = self.param_cont_scen["y_cell_size"] /2

        # Conversion to continuous[y,x]
        cont_coord = [ half_l - 2*coord[0]*half_c_size_y - half_c_size_y , -half_w + 2*coord[1]*half_c_size_x + half_c_size_x ]

        return cont_coord

    def compute_human_collision_binary(self,ext_traj,x_fb):
        # initial collision binary must be computed manually and then updated in the model

        hsm = self.params_from_env['human_security_margin']        
        P = np.matrix([[1.0,0.0],
                       [-1.0,0.0],
                       [0.0,1.0],
                       [0.0,-1.0]]) 
        
        q = np.array([  hsm/2.0,
                        hsm/2.0,
                        hsm/2.0,
                        hsm/2.0])
        
        n_obs_sides = len(q)
        n_robots = self.params_from_matlab['n_agents']-1

        B_col_0 = np.zeros([n_obs_sides,n_robots])
        for i in range(self.params_from_matlab['n_agents']-1):
            pos_i = np.array([x_fb[0,i],x_fb[2,i]])
            pos_human = np.array([ext_traj[0][1],ext_traj[0][0]])

            pos_diff = pos_i - pos_human
            for j in range(n_obs_sides):
                col_ineq = -P*np.matrix(pos_diff).T <= -np.matrix(q).T 
                B_col_0[j,i] = int(col_ineq.item(j))
            
        return B_col_0

    def update_mpc_feedback(self,ext_traj,x_fb,WTV):

        # parameters from model that are set in MATLAB
        # they are used to update the correct opt variables
        N = self.params_from_matlab['N']
        nx = self.params_from_matlab['nx']
        n_agents = self.params_from_matlab['n_agents']
        ny = self.params_from_matlab['ny']
        nu = self.params_from_matlab['nu']
        n_obs = self.params_from_matlab['n_obstacles']
        n_obs_sides = self.params_from_matlab['n_obs_sides']
        self.x_fb = x_fb.copy() # The feedback is used in the 'optimize' function also

        #### MPC STATE FEEDBACK ROBOTS
        for i in range(n_agents-1):
            ini_index = i*nx*(N+1)
            final_index = ini_index + nx
            indexes = range(ini_index,final_index)
            
            for j in range(nx):
                self.mpc_model.getVars()[indexes[j]].ub = x_fb.item(j,i)
                self.mpc_model.update()
                self.mpc_model.getVars()[indexes[j]].lb = x_fb.item(j,i)
                self.mpc_model.update()

        #### UPDATE TARGET VISITATION BOOLEAN
        for t in range(len(WTV)-1): # the wasTargetVisited vector in the yalmip model is the last one in the model
            self.mpc_model.getVars()[-(t+1)].ub = int(WTV[-(t+2)]) # skip WTV[-1] because its the last target
            self.mpc_model.update()
            self.mpc_model.getVars()[-(t+1)].lb = int(WTV[-(t+2)])
            self.mpc_model.update()

        #### UPDATE EXTERNAL AGENT TRAJECTORY
        offset_ext_agt = (N+1)*nx*(n_agents-1)
        for k  in range(N): 
            ini_index = offset_ext_agt + k*2
            final_index = offset_ext_agt + (k+1)*2
            indexes = range(ini_index,final_index)

            # r_x   
            self.mpc_model.getVars()[indexes[0]].ub = round(ext_traj[k][1],3)
            self.mpc_model.update()
            self.mpc_model.getVars()[indexes[0]].lb = round(ext_traj[k][1],3)
            self.mpc_model.update()

            # r_y
            self.mpc_model.getVars()[indexes[1]].ub = round(ext_traj[k][0],3)
            self.mpc_model.update()
            self.mpc_model.getVars()[indexes[1]].lb = round(ext_traj[k][0],3)
            self.mpc_model.update()

        # Last variable was not being set up correctly
        k = 30
        ini_index = offset_ext_agt + k*2
        final_index = offset_ext_agt + (k+1)*2
        indexes = range(ini_index,final_index)
                    # r_x   
        self.mpc_model.getVars()[indexes[0]].ub = round(ext_traj[k-1][1],3)
        self.mpc_model.update()
        self.mpc_model.getVars()[indexes[0]].lb = round(ext_traj[k-1][1],3)
        self.mpc_model.update()

            # r_y
        self.mpc_model.getVars()[indexes[1]].ub = round(ext_traj[k-1][0],3)
        self.mpc_model.update()
        self.mpc_model.getVars()[indexes[1]].lb = round(ext_traj[k-1][0],3)
        self.mpc_model.update()
     
        #### UPDATE horizon given human prediction
        #"""
        terminal_pos = self.discrete_to_continuous_coord(self.terminal_location)
        human_hor = 0
        for k in range(len(ext_traj)):
            if ext_traj[k] == terminal_pos:
                human_hor = k
                break
            else:
                human_hor = len(ext_traj)
        
        # resets last hor
        ind_offset = -((self.n_targets)+N)

        # resets last horizon
        self.mpc_model.getVars()[ind_offset+self.last_hor].ub = 0.0
        self.mpc_model.getVars()[ind_offset+self.last_hor].lb = 0.0

        # Sets new one
        self.mpc_model.getVars()[ind_offset+human_hor].ub = 1.0
        self.mpc_model.getVars()[ind_offset+human_hor].lb = 1.0

        self.last_hor = human_hor # store last horizon to reset to 0.0, otherwise there are two values equal 1.0 and the problem becomes infeasible
        """
        #### UPDATE human-robot collision binary for first time step
        
        b_col_offset = (N+1)*nx*(n_agents-1) + ny*(N+1) + 2*N*nu*(n_agents-1) + (N+1)*self.n_targets*(n_agents) + (N+1)*(n_agents-1)*n_obs*n_obs_sides
        
        self.mpc_model.getVars()[2011].ub = int(B_col_0[0,0])
        self.mpc_model.update()
        self.mpc_model.getVars()[2012].ub = int(B_col_0[1,0])
        self.mpc_model.update()
        self.mpc_model.getVars()[2013].ub = int(B_col_0[2,0])
        self.mpc_model.update()
        self.mpc_model.getVars()[2014].ub = int(B_col_0[3,0])
        self.mpc_model.update()

        self.mpc_model.getVars()[2256].ub = int(B_col_0[0,1])
        self.mpc_model.update()
        self.mpc_model.getVars()[2257].ub = int(B_col_0[1,1])
        self.mpc_model.update()
        self.mpc_model.getVars()[2258].ub = int(B_col_0[2,1])
        self.mpc_model.update()
        self.mpc_model.getVars()[2259].ub = int(B_col_0[3,1])
        self.mpc_model.update()
        """

        #for i in range(n_agents-1):
        
        #i=1
            #for j in range(n_obs_sides):
             #   indexes = range(b_col_offset + i*(n_agents-1)*(N+1)*n_obs_sides + n_obs_sides, b_col_offset + i*(n_agents-1)*(N+1)*n_obs_sides+n_obs_sides + n_obs_sides) 
              #  self.mpc_model.getVars()[indexes[j]].ub = int(B_col_0[j,i])
               # self.mpc_model.update()
                #self.mpc_model.getVars()[indexes[j]].lb = int(B_col_0[j,i])
               # self.mpc_model.update()
        #"""

    def optimize(self):

        self.mpc_model.optimize()
        code = self.mpc_model.status

        N = self.params_from_matlab['N']
        nx = self.params_from_matlab['nx']
        n_agents = self.params_from_matlab['n_agents']
        nu = self.params_from_matlab['nu']

        u = np.zeros([nu,N,n_agents-1])
        #u = np.zeros([nu,n_agents-1])
        if code == 4: # unfeasiable or unbounded

            for i in range(self.params_from_matlab['n_agents']-1):
                vxi = self.x_fb.item(1,i)
                vyi = self.x_fb.item(3,i)
                #u[:,i] = np.array([-2*vxi,-2*vyi]) # stop robots
                u[:,0,i] = np.array([-2*vxi,-2*vyi]) # stop robots
                trajs = np.zeros([nx,1,n_agents-1])
            flag = "Unfeasible"
        else:            
            self.sol_vec.append(self.mpc_model.X)

            # The vector of inputs is the third optimization variable (as set in MATLAB)
            # X = [X ; X_ext; U ; ...]
            # Thus the offset is nx*(N+1)*(n_agents-1) + 2*(N+1)

            # recover optimal horizon
            # X = [...,B_hor,B_wasTargetVisited]
            # Thus B_hor goes from [-(n_targets+N) , -(n_targets+1)]
            B_hor = np.array(self.mpc_model.X[-((self.n_targets)+N):-((self.n_targets-1))])
            self.opt_hor = np.where(B_hor == 1.0)[0][0]
            self.opt_hor = self.opt_hor + 1
            #print('Optimal horizon: ', self.opt_hor)
            #print('Cost: ', self.mpc_model.objVal)


            offset = nx*(N+1)*(n_agents-1) + 2*(N+1) # robots + human states
            trajs = np.zeros([nx,self.opt_hor,n_agents-1])
            for i in range(n_agents-1): # controls are not computed for expert
                #idx = offset + i*nu*N
                #u[:,i] = np.array(self.mpc_model.X[idx:idx+2]) # extract control

                # extract planned trajectories of each robot for plots
                for k in range(self.opt_hor):
                    # extract control sequence
                    idx = offset + k*nu + i*nu*N

                    # ignores values after maximum horizon
                    # Maximum horizon is extraploated when the prediciton bugs (agent keeps hoping between the same states)
                    # Some very particular situations unfortnately trigger this bug, butm typically, after the human moves the prediction model progresses
                    if k <= N-1: 
                        u[:,k,i] = np.array(self.mpc_model.X[idx:idx+2]) # extract control
                        
                        #vxi = self.x_fb.item(1,i)
                        #vyi = self.x_fb.item(3,i)
                        #u[:,k,i] = np.array([-2*vxi,-2*vyi]) # stop robots
                    #else:
                        #u[:,k,i] = np.array(self.mpc_model.X[idx:idx+2]) # extract control
    

                    # extract planned trajectory
                    ini_index = i*nx*(N+1) + k*nx
                    final_index = ini_index + nx 
                    trajs[:,k,i] = np.array(self.mpc_model.X[ini_index:final_index])

            # Simplistic optimziation flag    
            if code == 2:
                flag = "Optimal"
            else:
                flag = "Feasible"

        u_k = np.zeros([nu,n_agents-1])
        for i in range(n_agents-1):
            u_k[:,i] = u[:,0,i]

        return u_k,u,trajs,flag