from tqdm import tqdm
from ppo import PPO, TrajectoryDataset, update_policy

from stable_baselines3.common.vec_env import SubprocVecEnv
#from gym_wrapper import *
from airl import *
import torch
import numpy as np
import pickle
import wandb
import gym_examples
import gym
from stable_baselines3.common.utils import set_random_seed
import optuna
from optuna.trial import TrialState
from datetime import datetime
import os
import shutil

# Define version of AIRL
version = "random_env"

# Device Check
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def objective(trial, save_data=True):
    # REQUIRED FOR MULTI-THREADING
    def make_env(env_id, rank, seed=0):
        """
        Utility function for multiprocessed env.
    
        :param env_id: (str) the environment ID
        :param seed: (int) the inital seed for RNG
        :param rank: (int) index of the subprocess  
        """
        def _init():
            env = gym.make(env_id)

            # Important: use a different seed for each environment
            env.seed(seed + rank)
            return env
        set_random_seed(seed)
        return _init

    # Load demonstrations
    expert_trajectories = pickle.load(open('./demos/expert_policy_' + version + '.pk', 'rb')) 
    
    bs_disc_pwr = trial.suggest_int("batchsize_discriminator", 7, 10)
    bs_ppo_pwr = trial.suggest_int("batchsize_ppo", 4, 6)

    # Init WandB & Parameters
    current_run = wandb.init(project='AIRL_HP_tuning_'+version, config={
        'env_id': 'small_multitarget_v0',
        'env_steps': 10000000,
        'n_workers': 8, 
        'batchsize_discriminator': 2**bs_disc_pwr, 
        'batchsize_ppo': 2**bs_ppo_pwr, 
        'entropy_reg': trial.suggest_float("entropy_reg", 0.01, 0.05),
        'gamma': trial.suggest_float("gamma", 0.899, 0.999),
        'epsilon': trial.suggest_float("epsilon", 0.05, 0.15),
        'ppo_epochs': trial.suggest_int("ppo_epochs", 3, 7)
    })
    config = current_run.config

    # Create Environment
    vec_env = SubprocVecEnv([make_env("gym_examples/SmallMuseumCollect-v0", i) for i in range(config.n_workers)]) 

    states = vec_env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, n_actions=n_actions, in_channels=in_channels).to(device)
    discriminator = DiscriminatorMLP(state_shape=state_shape, in_channels=in_channels).to(device)

    optimizer_lr=trial.suggest_float("lr_o", 1e-4, 5e-4)
    discriminator_lr=trial.suggest_float("lr_d", 1e-4, 5e-4)
    # optimizer_lr=4e-4
    # discriminator_lr=4e-4
    optimizer = torch.optim.Adam(ppo.parameters(), lr=optimizer_lr) # 4e-4
    optimizer_discriminator = torch.optim.Adam(discriminator.parameters(), lr=discriminator_lr) # 4e-4
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    # Logging
    objective_logs = []
    prev_50_returns = []
    prev_best_returns = -np.inf

    # Save the model and optimizer data locally?
    if save_data:
        f = open(version+"_logging.txt", "a")
        f.write(f"Trial {trial.number}, current hps in training:\n")
        f.write(f"entropy_reg: {config.entropy_reg}\t gamma: {config.gamma}\t epsilon: {config.epsilon}\t \
                ppo_epochs: {config.ppo_epochs}\t lr_o: {optimizer_lr}\t lr_d: {discriminator_lr}\t \
                bs_disc: {config.batchsize_discriminator}\t bs_ppo: {config.batchsize_ppo}\n")

    for t in tqdm(range((int(config.env_steps/config.n_workers)))):
        
        actions, log_probs = ppo.act(states_tensor)

        next_states, rewards, dones, _  = vec_env.step(actions)

        # Data treatment
        next_states = np.stack(next_states, axis=0) # correct format

        # Log Objectives
        objective_logs.append(rewards)

        # Calculate (vectorized) AIRL reward
        airl_state = torch.tensor(states).to(device).float()
        airl_next_state = torch.tensor(next_states).to(device).float()
        airl_action_prob = torch.exp(torch.tensor(log_probs)).to(device).float()

        for i in range(config.n_workers):
            #"""
            if not dones[i]:
                airl_rewards = discriminator.predict_reward(airl_state, airl_next_state, config.gamma, airl_action_prob) # \hat{r}
            else: # overwrittes reset from env to register zeroed reward matrix
                state_aux = states.copy()
                state_aux[0][1] = np.zeros(state_shape)
                state_aux[0][2] = np.zeros(state_shape)
                test_airl_state = torch.tensor(state_aux).to(device).float()
                airl_rewards = discriminator.predict_reward(airl_state, test_airl_state, config.gamma, airl_action_prob)
            #"""

        airl_rewards = list(airl_rewards.detach().cpu().numpy() * [0 if i else 1 for i in dones])

        # Save Trajectory
        train_ready = dataset.write_tuple(states, actions, airl_rewards, dones, log_probs, rewards) # GENERATOR trajectories

        if train_ready:

            #Hack: the movement penalization log keeps registering penalizations after the done signal (only a logging problem)
            objective_logs = dataset.log_objectives()
            for j in range(config.batchsize_ppo):
                traj_length = len(dataset.trajectories[j]['rewards'])
                objective_logs[j, 0] = rewards[0,0]*traj_length
            # END OF hack SOLUTION

            current_run.log({'Movement penalties' : objective_logs[:, 0].mean()})
            current_run.log({'Collision penalties' : objective_logs[:, 3].mean()})
            current_run.log({'Target A rewards' : objective_logs[:, 1].mean()})
            current_run.log({'Target B rewards' : objective_logs[:, 2].mean()})

            objective_logs = []
            
            # Update Models
            update_policy(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs,
                          entropy_reg=config.entropy_reg)
            d_loss, fake_acc, real_acc = update_discriminator(discriminator=discriminator,
                                                              optimizer=optimizer_discriminator,
                                                              gamma=config.gamma,
                                                              expert_trajectories=expert_trajectories,
                                                              policy_trajectories=dataset.trajectories.copy(), ppo=ppo,
                                                              batch_size=config.batchsize_discriminator)

            # Log Loss Statsitics
            current_run.log({'Discriminator Loss': d_loss,
                       'Fake Accuracy': fake_acc,
                       'Real Accuracy': real_acc})
            
            for ret in dataset.log_returns():
                current_run.log({'Returns': ret})

                # Save 50 reward values after the 10000th itteration
                if (t%10000 < 50) and save_data:
                    prev_50_returns.append(ret)

            dataset.reset_trajectories()

            # Save every 10000th best avg model based on window of 50 previous returns
            if (t%10000 == 0) and save_data and (len(prev_50_returns) > 0):
                current_avg_returns = sum(prev_50_returns)/len(prev_50_returns)
                if (current_avg_returns > prev_best_returns):
                    prev_best_returns = current_avg_returns

                    # Add to text file itteration and returns
                    f.write(f"Itt {t}, prev 50 return average: {current_avg_returns}, at {datetime.now().strftime("%d/%m/%Y %H:%M")}\n")
                prev_50_returns = []

        # This is a continuation of the sampling part
        # Prepare state input for next time step
        if not train_ready:
            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)

    vec_env.close()
    current_run.finish()
    f.close()

    # Delete local wandb run to save storage
    for f in os.listdir("./wandb"):
        if(f[:3] == 'run'):
            shutil.rmtree("./wandb/" + f)

    return prev_best_returns

if __name__ == '__main__':

    study = optuna.create_study(direction="maximize")
    study.optimize(objective, n_trials=30)

    f = open(version+"_logging.txt", "a")
    f.write("Study statistics:\n")
    f.write(f"    Number of finished trials: {len(study.trials)}\n")

    f.write("Best trial:\n")
    trial = study.best_trial

    f.write(f"  Value: {trial.value}\n")
    f.write("  Params: \n")
    for key, value in trial.params.items():
        f.write("    {}: {}\n".format(key, value))
    f.close()