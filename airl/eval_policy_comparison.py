from tqdm import tqdm
from ppo import *
import torch
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from airl import *
import gym
import gym_examples
import pickle

# Use GPU if available
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

def simulate_policy(n_demos=1000, policy_file=""):
    
    # Create Environment
    env_id = 'SmallMuseumCollect-v0'
    env = gym.make("gym_examples/"+env_id)
    states = env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = env.action_space.n
    obs_shape = env.observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]

    max_steps = 45

    # PPO
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    ppo.load_state_dict(torch.load('./saved_policies/' + policy_file + '.pt'))
    
    # 6 results: obstacle id, traj length, tar A collection, tar B collection, collisions, returns
    # 1000 demos
    # Average to be taken at the end
    results = np.zeros((1000, 6))

    for i in range(n_demos):
        print(i)

        # New row for current env
        r_i = np.zeros(6)
        r_i[0] = env.obs_id
        done = False

        # Dynamics
        while not done:
            actions, _ = ppo.act(states_tensor)
            next_states, rewards, done, _ = env.step(actions[0]) # done is related to 'k' (states) not 'k+1' (next_states)
            next_states = np.stack(next_states, axis=0)
            
            # Track any target collection and collision
            if rewards[1] != 0.0: r_i[2]+=1
            if rewards[2] != 0.0: r_i[3]+=1
            if rewards[3] != 0.0: r_i[4]+=1

            r_i[1] += 1
            r_i[5] += np.sum(rewards)

            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)

        results[i, :] = r_i.copy()

        states = env.state.copy()
        states_tensor = torch.tensor(states).float().to(device)

    return results

if __name__ == '__main__':
    results = simulate_policy(policy_file="airl_policy_random_env_max")
    #results = simulate_policy(policy_file="expert_policy_random_env_max")

    print(f"Means over 1000: {np.mean(results, axis=0)}")
    print(f"Confidence interval min: {np.min(results, axis=0)}")
    print(f"Confidence interval max: {np.max(results, axis=0)}")

    fail_mask = (results[:, 1] >= 45) | (results[:, 1] == results[:, 4])
    failed = results[fail_mask, :]
    success = results[~fail_mask, :]

    print(f"Number of fails: {len(failed)}")
    print(f"Number of success: {len(success)}")

    print(f"Means over success: {np.mean(success, axis=0)}")
    print(f"Confidence interval min: {np.min(success, axis=0)}")
    print(f"Confidence interval max: {np.max(success, axis=0)}")

    # Get mean per environment id
    for i in range(1,6):
        print(f"Means for successfull id: {i} = {np.mean(success[(success[:,0]==i)], axis=0)}")