from tqdm import tqdm
from ppo import *
import torch
#from gym_wrapper import *
import wandb
import argparse
import os
import pickle
from matplotlib import pyplot as plt
import gym_examples
import gym
import sys
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.utils import set_random_seed
print(sys.executable)

"""
This code is a modification
"""

# Use GPU if available
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

if __name__ == '__main__':
    # REQUIRED FOR MULTI-THREADING
    def make_env(env_id, rank, seed=0):
        """
        Utility function for multiprocessed env.
    
        :param env_id: (str) the environment ID
        :param seed: (int) the inital seed for RNG
        :param rank: (int) index of the subprocess  
        """
        def _init():
            env = gym.make(env_id)

            # Important: use a different seed for each environment
            env.seed(seed + rank)
            return env
        set_random_seed(seed)
        return _init

    # Init WandB & Parameters
    wandb.init(project='PPO', config={
        'env_id': 'small_museum_collect_v0',
        'env_steps': 32000000,
        'batchsize_ppo': 32,
        'n_workers': 8,
        'lr_ppo': 3e-4,
        'entropy_reg': 0.03,
        'lambd': [1.0, 1.0, 1.0, 1.0],
        'gamma': 0.999,
        'epsilon': 0.1,
        'ppo_epochs': 5
    })
    config = wandb.config
    
    # Create Environments
    #vec_env  = [gym.make("gym_examples/SimpleGrid-v0") for i in range(config.n_workers)]
    #vec_env  = [gym.make("gym_examples/AIRLDemo-v0") for i in range(config.n_workers)]
    vec_env = SubprocVecEnv([make_env("gym_examples/SmallMuseumCollect-v0", i) for i in range(config.n_workers)]) 

    # Generate random grids and get states
    states = vec_env.reset()

    states = np.stack(states, axis=0)
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch shapes
    n_actions = vec_env.action_space.n
    obs_shape = vec_env.observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    optimizer = torch.optim.Adam(ppo.parameters(), lr=config.lr_ppo)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    for t in tqdm(range(int(config.env_steps / config.n_workers))):

        # Sampling ----------------------------------------------------------------------------------
        actions, log_probs = ppo.act(states_tensor)

        next_states, rewards, dones, _  = vec_env.step(actions)

        # Data treatment
        next_states = np.stack(next_states, axis=0) # correct format

        scalarized_rewards = np.sum(np.multiply(config.lambd, rewards), axis=1)

        # Save trajectories for ppo to sample
        train_ready = dataset.write_tuple(states, actions, scalarized_rewards, dones, log_probs, rewards)

        # PPO ---------------------------------------------------------------------------------
        # This runs only when enough demonstrations are gathered (see write_tuple method)
        if train_ready:
            update_policy(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs,
                          entropy_reg=config.entropy_reg)
            objective_logs = dataset.log_objectives()
            #for i in range(objective_logs.shape[1]):
                #wandb.log({'Obj_' + str(i): objective_logs[:, i].mean()})
            wandb.log({'Movement penalties': objective_logs[:, 0].mean()})
            wandb.log({'Target A collection': objective_logs[:, 1].mean()})
            wandb.log({'Target B collection': objective_logs[:, 2].mean()})
            wandb.log({'Collision penalties': objective_logs[:, 3].mean()})

            for ret in dataset.log_returns():
                wandb.log({'Returns': ret})
            dataset.reset_trajectories()
        # End PPO ---------------------------------------------------------------------------------
        
        # This is a continuation of the sampling part
        # Prepare state input for next time step
        if not train_ready:
            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)


    torch.save(ppo.state_dict(), "./saved_policies/expert_policy_random_env_max.pt")
