from cProfile import Profile
from pstats import SortKey, Stats

from airl_train_plot import AnalyseResults

if __name__ == '__main__':
    with Profile() as profile:
        print(f"{AnalyseResults(14, 14).train_model(version="random_env", create_gif=False) = }")
        (
            Stats(profile)
            .strip_dirs()
            .sort_stats(SortKey.CALLS)
            .print_stats()
        )   