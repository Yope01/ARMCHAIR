 

import pickle
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import bootstrap
from tabulate import tabulate

file = open('./monte_carlo_policy_eval/table_3_data.pk', 'rb')

metrics_per_env, ci_for_picke = pickle.load(file)

ci_lower_expert = ci_for_picke['ci_lower_expert']
ci_upper_expert = ci_for_picke['ci_upper_expert']
ci_lower_AIRL = ci_for_picke['ci_lower_AIRL']
ci_upper_AIRL = ci_for_picke['ci_upper_AIRL']

n_demos = len(metrics_per_env)-1


# Averages and CI
avg_ep_length_per_ep = list(map(lambda x: x["avg_ep_length"], metrics_per_env))
avg_n_tar_A_visit_per_ep = list(map(lambda x: x['avg_n_tar_A_visit'], metrics_per_env))
avg_n_tar_B_visit_per_ep = list(map(lambda x: x['avg_n_tar_B_visit'], metrics_per_env))
avg_n_coll_per_ep = list(map(lambda x: x['avg_n_coll'], metrics_per_env))
avg_return_per_ep = list(map(lambda x: x['avg_return'], metrics_per_env))

 # Expert
avg_ep_lengh_expert = np.mean(avg_ep_length_per_ep[0:int(n_demos/2)])
avg_n_tar_A_visit_expert = np.mean(avg_n_tar_A_visit_per_ep[0:int(n_demos/2)])
avg_n_tar_B_visit_expert = np.mean(avg_n_tar_B_visit_per_ep[0:int(n_demos/2)])
avg_n_coll_expert = np.mean(avg_n_coll_per_ep[0:int(n_demos/2)])
avg_return_expert = np.mean(avg_return_per_ep[0:int(n_demos/2)])

ci_avg_ep_length_expert= bootstrap( (avg_ep_length_per_ep[0:int(n_demos/2)],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_n_tar_A_visit_expert = bootstrap( (avg_n_tar_A_visit_per_ep[0:int(n_demos/2)],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_n_tar_B_visit_expert = bootstrap( (avg_n_tar_B_visit_per_ep[0:int(n_demos/2)],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_n_coll_expert = bootstrap( (avg_n_coll_per_ep[0:int(n_demos/2)],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_return_expert = bootstrap( (avg_return_per_ep[0:int(n_demos/2)],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')

 # AIRL
avg_ep_lengh_AIRL = np.mean(avg_ep_length_per_ep[int(n_demos/2):-1])
avg_n_tar_A_visit_AIRL = np.mean(avg_n_tar_A_visit_per_ep[int(n_demos/2):-1])
avg_n_tar_B_visit_AIRL = np.mean(avg_n_tar_B_visit_per_ep[int(n_demos/2):-1])
avg_n_coll_AIRL = np.mean(avg_n_coll_per_ep[int(n_demos/2):-1])
avg_return_AIRL = np.mean(avg_return_per_ep[int(n_demos/2):-1])

ci_avg_ep_length_AIRL= bootstrap( (avg_ep_length_per_ep[int(n_demos/2):-1],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_n_tar_A_visit_AIRL = bootstrap( (avg_n_tar_A_visit_per_ep[int(n_demos/2):-1],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_n_tar_B_visit_AIRL = bootstrap( (avg_n_tar_B_visit_per_ep[int(n_demos/2):-1],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_n_coll_AIRL = bootstrap( (avg_n_coll_per_ep[int(n_demos/2):-1],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')
ci_avg_return_AIRL = bootstrap( (avg_return_per_ep[int(n_demos/2):-1],) ,np.mean,confidence_level=0.95,random_state=1,method='percentile')

table = [
    [' ', 'avg','CI_lower','CI_upper'],
    ['ep_length_expert', avg_ep_lengh_expert, ci_avg_ep_length_expert.confidence_interval[0],ci_avg_ep_length_expert.confidence_interval[1]],
    ['ep_length_AIRL', avg_ep_lengh_AIRL, ci_avg_ep_length_AIRL.confidence_interval[0],ci_avg_ep_length_AIRL.confidence_interval[1]],
    [' ', ' ',' ',' '],
    ['tarA_visit_expert', avg_n_tar_A_visit_expert, ci_avg_n_tar_A_visit_expert.confidence_interval[0],ci_avg_n_tar_A_visit_expert.confidence_interval[1]],
    ['tarA_visit_AIRL', avg_n_tar_A_visit_AIRL , ci_avg_n_tar_A_visit_AIRL.confidence_interval[0],ci_avg_n_tar_A_visit_AIRL.confidence_interval[1]],
    [' ', ' ',' ',' '],
    ['tarB_visit_expert', avg_n_tar_B_visit_expert, ci_avg_n_tar_B_visit_expert.confidence_interval[0],ci_avg_n_tar_B_visit_expert.confidence_interval[1]],
    ['tarB_visit_AIRL', avg_n_tar_B_visit_AIRL , ci_avg_n_tar_B_visit_AIRL.confidence_interval[0],ci_avg_n_tar_B_visit_AIRL.confidence_interval[1]],
    [' ', ' ',' ',' '],
    ['n_coll_expert', avg_n_coll_expert, ci_avg_n_coll_expert.confidence_interval[0],ci_avg_n_coll_expert.confidence_interval[1]],
    ['n_coll_AIRL', avg_n_coll_AIRL , ci_avg_n_coll_AIRL.confidence_interval[0],ci_avg_n_coll_AIRL.confidence_interval[1]],
    [' ', ' ',' ',' '],
    ['return_expert', avg_return_expert, ci_avg_return_expert.confidence_interval[0],ci_avg_return_expert.confidence_interval[1]],
    ['return_AIRL', avg_return_AIRL , ci_avg_return_AIRL .confidence_interval[0],ci_avg_return_AIRL.confidence_interval[1]],
]
print(tabulate(table))

# Plots
plt.figure(1)
plt.plot(avg_ep_length_per_ep[0:int(n_demos/2)],"-b",label='Expert')
plt.plot(avg_ep_length_per_ep[int(n_demos/2):-1],"--r",label='AIRL')
plt.legend(loc="upper right")
plt.grid()
plt.xticks(np.arange(0, n_demos/2, step=int(n_demos/5)))
plt.xlabel('Environment')
plt.ylabel('Average Trajectory Length')
plt.savefig('ep_length_results.pdf',format="pdf",bbox_inches="tight")

plt.figure(2)
plt.plot(avg_n_tar_A_visit_per_ep[0:int(n_demos/2)],"-b",label='Expert')
plt.plot(avg_n_tar_A_visit_per_ep[int(n_demos/2):-1],"--r",label='AIRL')
plt.legend(loc="upper right")
plt.grid()
plt.xticks(np.arange(0, n_demos/2, step=int(n_demos/5)))
plt.xlabel('Environment')
plt.ylabel('Average Target A Collection')
plt.ylim(-1,4)
plt.savefig('tar_A_results.pdf',format="pdf",bbox_inches="tight")

plt.figure(3)
plt.plot(avg_n_tar_B_visit_per_ep[0:int(n_demos/2)],"-b",label='Expert')
plt.plot(avg_n_tar_B_visit_per_ep[int(n_demos/2):-1],"--r",label='AIRL')
plt.legend(loc="upper right")
plt.grid()
plt.xticks(np.arange(0, n_demos/2, step=int(n_demos/5)))
plt.xlabel('Environment')
plt.ylabel('Average Target B Collection')
plt.ylim(-1,4)
plt.savefig('tar_B_results.pdf',format="pdf",bbox_inches="tight")

plt.figure(4)
plt.plot(avg_n_coll_per_ep[0:int(n_demos/2)],"-b",label='Expert')
plt.plot(avg_n_coll_per_ep[int(n_demos/2):-1],"--r",label='AIRL')
plt.legend(loc="upper right")
plt.grid()
plt.xticks(np.arange(0, n_demos/2, step=int(n_demos/5)))
plt.xlabel('Environment')
plt.ylabel('Average Collisions')
plt.ylim(-1,4)
plt.savefig('collision_results.pdf',format="pdf",bbox_inches="tight")

plt.figure(5)
x = np.linspace(0,(n_demos/2)-1,int(n_demos/2))
y = avg_return_per_ep[0:int(n_demos/2)]

plt.plot(x,y,"-b",label='Expert')
plt.fill_between(x,ci_lower_expert,ci_upper_expert,color='b',alpha=0.5)

y = avg_return_per_ep[int(n_demos/2):-1]
plt.plot(x,y,"--r",label='AIRL')
plt.fill_between(x,ci_lower_AIRL,ci_upper_AIRL,color='r',alpha=0.5)

plt.legend(loc="upper right")
plt.grid()
plt.xticks(np.arange(0, n_demos/2, step=int(n_demos/5)))
plt.xlabel('Environment')
plt.ylabel('Average Return')
#plt.ylim(-1,4)
plt.savefig('return_results.pdf',format="pdf",bbox_inches="tight")

plt.show()