import gym
from gym import spaces
import pygame
import numpy as np
import matplotlib as mpl
import matplotlib.cm as cm
import itertools
from gym.utils import seeding

class SmallMuseumCollect(gym.Env):
    #metadata = {"render_modes": ["human", "rgb_array"], "render_fps": 1000}

    def __init__(self):
   
        
        self.size_hor = 14
        self.size_ver = 14
        self.window_size_hor = 853  # The size of the PyGame window
        self.window_size_ver = 512  # The size of the PyGame window

        # Actions/direction mapping
        self._directions = [
            [0,1],
            [-1,1],
            [-1,0],
            [-1,-1],
            [0,-1],
            [1,-1],
            [1,0],
            [1,1]
        ] # Diagonal steps cover more distance, but cannot normalize due to grid size.

        # Agent, Obstacle, Wall, Target A, and Target B
        self.n_layers = 5

        self.n_actions = len(self._directions)

        self.action_space = spaces.Discrete(self.n_actions)

        self.observation_space = spaces.Box(
            low=0, high=1,
            shape=(self.n_layers, self.size_ver, self.size_hor ),
            dtype=np.uint8
        )

        # For testing purposes change from 1/4 to 4/10
        self.max_n_targets = 6
        self.min_n_targets = 2
        self.reset_counter = 0

        # Rewards
        self.mov_penalty = -0.1
        self.diag_mov_penalty = -0.14
        self.coll_penalty = -1.0
        self.tar_A_reward = 0.5
        self.tar_B_reward = 1.0

        # max time steps
        self.max_timesteps = 45

        # determines if scenario should be randomized or only restarted when reset
        self.is_rng_scen = False
        self.ini_state = []

        self.seed()
        self.reset()

        # Grid plot stuff
        #assert render_mode is None or render_mode in self.metadata["render_modes"]
        #self.render_mode = render_mode

        """
        If human-rendering is used, `self.window` will be a reference
        to the window that we draw to. `self.clock` will be a clock that is used
        to ensure that the environment is rendered at the correct framerate in
        human-mode. They will remain `None` until human-mode is used for the
        first time.
        """
        self.window = None
        self.clock = None


    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]
    # ------------------------------
    def _set_targets(self,tar_A_idx, tar_B_idx):
        self.tar_A_list = tar_A_idx
        self.tar_B_list = tar_B_idx    

    # ------------------------------
    def _set_obstacles(self):

        # Randomize between 4 predefined obstacle fields
        n_obs_fields = 5
        self.obs_id = np.random.randint(1, n_obs_fields+1)

        # Set obstacle cells
        self.obs_list = []
        self._get_obs_list(self.obs_id)   

        self.n_obs = len(self.obs_list)
            
    # ------------------------------
    def _build_states(self):
        """
        This method builds the states as layers of different features.
        Each layer is represented by a binary matrix {0,1}^{size_nx \times size_ny}

            s = [M_agt, M_tar_A, M_tar_B, M_obs]
            
        """

        # Agent matrix
        M_agt = np.zeros((self.size_ver,self.size_hor))
        M_agt[self.ini_agent_location[0],self.ini_agent_location[1]] = 1.0 

        # Target A matrix
        M_tar_A = np.zeros((self.size_ver,self.size_hor))
        for i in range(self.n_tar_A):
            M_tar_A[self.tar_A_list[i][0],self.tar_A_list[i][1]] = 1.0
        
        # Target B matrix
        M_tar_B = np.zeros((self.size_ver,self.size_hor))
        for i in range(self.n_tar_B):
            M_tar_B[self.tar_B_list[i][0],self.tar_B_list[i][1]] = 1.0

        # Terminal state matrix
        M_ter = np.zeros((self.size_ver,self.size_hor))
        M_ter[self.terminal_location[0],self.terminal_location[1]] = 1.0

        # obs matrix
        M_obs = np.zeros((self.size_ver,self.size_hor))
        for i in range(self.n_obs):
            M_obs[self.obs_list[i][0],self.obs_list[i][1]] = 1.0

        state = np.array([M_agt,M_tar_A,M_tar_B,M_obs,M_ter], dtype=np.float32)
       
        return state       

    def get_obs(self):
        return {"agent": self._agent_location, "target_binaries": self.target_binaries}

    def _get_info(self):
        return {
            "distance": np.linalg.norm(
                self._agent_location - np.array([0,0]), ord=1
            )
        }

    def idx_to_scalar(self,idx):
        """
        0 1 2
        3 4 5
        6 7 8
        9 10 11

        Maps (2,2) -> 8 (upper left going downwards)
             (3,1) -> 10

        This is done to facilitate matrix manipulation afterwards
        """
        # This logic does not work if the idx is out of bounds
        # Therefore, we return -1 in this case
        scalar = []
        for i in idx:
            if 0.0 <= i[0] < self.size_ver and 0.0 <= i[1] < self.size_hor: #within bounds
                scalar.append( (self.size_hor)*i[0] + i[1] )
            else:
                scalar.append( -1 )

        #scalar = [(self.size_hor)*i[0] + i[1] for i in idx]

        return scalar

    def scalar_to_idx(self,n):
        x = int(np.floor(n/self.size_hor))
        y = int(n - np.floor(n/self.size_hor)*self.size_hor)
        return x,y

    def refresh_reset_counter(self):
        self.reset_counter = 0

    def update_seed(self):
        self.reset_counter = self.reset_counter + 1

    def reset(self):

        # Flags for montecarlo validation
        self.flag_tar_A = False
        self.flag_tar_B = False
        self.flag_coll = False

        # Resets timer
        self.time_step = 0

        # Obstacle objects coordinates are fixed in this environment
        self._set_obstacles()

        obs_scalar = self.idx_to_scalar(self.obs_list)
        aval_idx = list(range(self.size_hor*self.size_ver))
        aval_idx_no_obs = [x for x in aval_idx if x not in obs_scalar] 
        
        # Randomize env objects coordinates
        rng_scalars = np.random.choice(aval_idx_no_obs, size=self.size_hor+self.size_ver+2, replace=False) # +2 (targets+ini_pos+termination_state)

        # rng terminal position
        self.terminal_location = self.scalar_to_idx(rng_scalars[-1])

        # rng initial position
        self.ini_agent_location = self.scalar_to_idx(rng_scalars[0])
        #self.ini_agent_location = (2,1)
        self._agent_location = self.ini_agent_location
        
        # rng total and specific number of targets
        total_n_targets = np.random.choice(range(self.min_n_targets,self.max_n_targets+1), size=1, replace=False)[0]
        self.n_tar_A = np.random.choice(range(total_n_targets+1), size=1, replace=False)[0]
        self.n_tar_B = total_n_targets - self.n_tar_A
  
        # rng target A and B positions
        tar_A_idx = [self.scalar_to_idx(i) for i in rng_scalars[1:1+self.n_tar_A]]
        tar_B_idx = [self.scalar_to_idx(i) for i in rng_scalars[self.n_tar_A+1:self.n_tar_A+1+self.n_tar_B]]

        self._set_targets(tar_A_idx,tar_B_idx)

        # Builds layered
        self.ini_state = self._build_states()
        self.state = self.ini_state
        observation = self.ini_state

        #if self.render_mode == "human":
            #self.render_frame()

        self.update_seed()

        return observation

    def update_state(self,new_state):
        self.state = new_state.copy()

    def step(self, action):

        # test montecarlo
        self.flag_tar_A = False
        self.flag_tar_B = False
        self.flag_coll = False

        # Start by determining agent location
        agt_loc_idx  = [np.where(self.state[0] == 1.0)[0][0],np.where(self.state[0] == 1.0)[1][0]]

        # Each steps generates a time/movement penalization
        # Diagonal movement penalized more due to more space covered
        if action % 2 > 0:
            self.reward = [self.diag_mov_penalty, 0.0, 0.0, 0.0]
        else:
            self.reward = [self.mov_penalty, 0.0, 0.0, 0.0]

        # Verifies step reached terminal state
        if np.array_equal(agt_loc_idx,self.terminal_location):
            self.update_state(self.reset())
            next_state = self.state.copy()
            self.time_step = self.time_step + 1
            return next_state, self.reward, True, { 'test:': 1 } 
        
        # Verify time step maxed out
        if self.time_step > self.max_timesteps:
            self.reward[0] = -20
            self.update_state(self.reset())
            next_state = self.state.copy()
            self.time_step = self.time_step + 1
            return next_state, self.reward, True, { 'test:': 1 } 

        # Verifies if one of targets A was visited
        if self.state[1][agt_loc_idx[0],agt_loc_idx[1]] == 1.0:
            self.reward[1] = self.tar_A_reward
            self.flag_tar_A = True

        # Verifies if one of targets B was visited
        if self.state[2][agt_loc_idx[0],agt_loc_idx[1]] == 1.0:
            self.reward[2] = self.tar_B_reward
            self.flag_tar_B = True

        # Verifies if collision happened
        if self.state[3][agt_loc_idx[0],agt_loc_idx[1]] == 1.0:
            self.reward[3] = self.coll_penalty
            self.flag_coll = True

        # Initialize next state as a copy of current state
        next_state = self.state.copy()

        # Start dynamics by potentially moving the agent
        # action = 0 -> right
        # action = 2 -> down
        # action = 4 -> left
        # action = 6 -> up
        # Diagonal movements between
        agt_new_loc_idx = agt_loc_idx + np.array(self._directions[action])

        # Verifies if location is reachable; it is not reachable if outside bounds or on an obstacle space
        # The method idx_to_scalar already verifies if index is within bounds of the grid (-1 flags out of bounds)
        if self.idx_to_scalar([agt_new_loc_idx])[0] >= 0 and not self.state[3][agt_new_loc_idx[0],agt_new_loc_idx[1]] == 1.0:
            next_state[0][agt_loc_idx[0],agt_loc_idx[1]] = 0.0
            next_state[0][agt_new_loc_idx[0],agt_new_loc_idx[1]] = 1.0
        else:
            self.reward[3] = -1.0 # If collision with wall, -1

        # Verifies if one tar_A was visited and removes it from M_tar_A
        if self.reward[1] > 0.0:
            next_state[1][agt_loc_idx[0],agt_loc_idx[1]] = 0.0

        # Verifies if one tar_B was visited and removes it from M_tar_B
        if self.reward[2] > 0.0:
            next_state[2][agt_loc_idx[0],agt_loc_idx[1]] = 0.0

        self.update_state(next_state)
        self.time_step = self.time_step + 1

        return next_state, self.reward, False, { 'test:': 1 } # IF YOU DONT PUT A DICTIONARY IN THE 4TH SPOT, THE MULTITHREADING DOESNT WORK =(


    def render(self):
        if self.render_mode == "rgb_array":
            return self.render_frame()

    def render_frame(self):
        if self.window is None and self.render_mode == "human":
            pygame.init()
            pygame.display.init()
            self.window = pygame.display.set_mode((self.window_size_hor, self.window_size_ver))
        if self.clock is None and self.render_mode == "human":
            self.clock = pygame.time.Clock()

        canvas = pygame.Surface((self.window_size_hor, self.window_size_ver))
        canvas.fill((255, 255, 255))
        pix_square_size_hor = (
            self.window_size_hor / self.size_hor
        )  
        pix_square_size_ver = (
            self.window_size_ver / self.size_ver
        )  

        
        # The size of a single grid square in pixels

        # First we draw the target
        """
        pygame.draw.rect(
            canvas,
            (255, 0, 0),
            pygame.Rect(
                (pix_square_size_hor * self._target_location[0],pix_square_size_ver * self._target_location[1]),
                (pix_square_size_hor, pix_square_size_ver),
            ),
        )
        """
        for i in range(self.n_tar):
            for j in range(len(self.tar_list[i])):
                pygame.draw.rect(
                canvas,
                (255, 0, 0),
                 pygame.Rect(
                  (pix_square_size_hor  * self.tar_list[i][j][0],pix_square_size_ver  * self.tar_list[i][j][1]),
                  (pix_square_size_hor, pix_square_size_ver)
                  ),
        )


        # Now we draw the agent
        pygame.draw.circle(
            canvas,
            (0, 0, 255),
            ((self._agent_location[0] + 0.5) * pix_square_size_hor,(self._agent_location[1] + 0.5) * pix_square_size_ver), #position
            pix_square_size_ver / 3, # radius
        )

        # And the obstacles
        for i in range(len(self.obs_list)):
            for j in range(len(self.obs_list[i])):
                pygame.draw.rect(
                canvas,
                (0, 0, 0),
                 pygame.Rect(
                  (pix_square_size_hor  * self.obs_list[i][j][0],pix_square_size_ver  * self.obs_list[i][j][1]),
                  (pix_square_size_hor, pix_square_size_ver)
                  ),
        )

        # Finally, add some gridlines
        for y in range(self.size_ver + 1):
            pygame.draw.line(
                canvas,
                0,
                (0, pix_square_size_ver * y),
                (self.window_size_hor, pix_square_size_ver * y),
                width=3,
            )
        for x in range(self.size_hor + 1):
            pygame.draw.line(
                canvas,
                0,
                (pix_square_size_hor * x, 0),
                (pix_square_size_hor * x, self.window_size_hor),
                width=3,
            )

        if self.render_mode == "human":
            # The following line copies our drawings from `canvas` to the visible window
            self.window.blit(canvas, canvas.get_rect())
            pygame.event.pump()
            pygame.display.update()

            # We need to ensure that human-rendering occurs at the predefined framerate.
            # The following line will automatically add a delay to keep the framerate stable.
            self.clock.tick(self.metadata["render_fps"])
           
        else:  # rgb_array
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(canvas)), axes=(1, 0, 2)
            )

    def close(self):
        if self.window is not None:
            pygame.display.quit()
            pygame.quit()


    def num_to_rgb(self,val, max_val):
            """
            r = 255
            g = 255
            b = 255
            if val > 0:
                i = (round(val) * 255 / max_val)
                r = round(np.sin(0.024 * i + 4) * 127 + 128)
                g = round(np.sin(0.024 * i + 2) * 127 + 128)
                b = round(np.sin(0.024 * i + 0) * 127 + 128)
            return (r,g,b)
            """
            r = 1
            g = 1
            b = 1
            if val > 0:
                if val < 10:
                    val = 20
                norm = mpl.colors.Normalize(vmin=0, vmax=max_val)
                cmap = cm.Blues
                m = cm.ScalarMappable(norm=norm, cmap=cmap)
                r = m.to_rgba(val)[0]
                g = m.to_rgba(val)[1]
                b = m.to_rgba(val)[2]
            return (r*255,g*255,b*255)

    
    # show frequency maps 
    def show_freqmap(self,cell_freq):
        if self.window is None and self.render_mode == "human":
            pygame.init()
            pygame.display.init()
            self.window = pygame.display.set_mode((self.window_size_hor, self.window_size_ver))
        if self.clock is None and self.render_mode == "human":
            self.clock = pygame.time.Clock()

        canvas = pygame.Surface((self.window_size_hor, self.window_size_ver))
        canvas.fill((255, 255, 255))
        pix_square_size_hor = (
            self.window_size_hor / self.size_hor
        )  
        pix_square_size_ver = (
            self.window_size_ver / self.size_ver
        )  

        # Draw heat map       
        for i in range(self.size_hor ):
            for j in range(self.size_ver ):
                rgb_color = self.num_to_rgb(cell_freq[i][j],np.max(cell_freq))
                pygame.draw.rect(
                canvas,
                rgb_color,
                pygame.Rect(
                  (pix_square_size_hor  * i,pix_square_size_ver  * j),
                  (pix_square_size_hor, pix_square_size_ver)
                  ),
        )
          

        # draw frequency numbers
        font = pygame.font.SysFont('Arial', 15)
        for i in range(self.size_hor ):
            for j in range(self.size_ver ):
                
                if cell_freq[i,j] > 0:
                    img = font.render(  str(int(cell_freq[i,j])), True, (0,255,0))
                    canvas.blit(img, (i*1*pix_square_size_hor,j*1*pix_square_size_ver))

        for i in range(len(self.tar_list)):
            for j in range(len(self.tar_list[i])):
                pygame.draw.rect(
                canvas,
                (255, 0, 0),
                 pygame.Rect(
                  (pix_square_size_hor  * self.tar_list[i][j][0],pix_square_size_ver  * self.tar_list[i][j][1]),
                  (pix_square_size_hor, pix_square_size_ver)
                  ),
        )


        # Now we draw the agent
        pygame.draw.circle(
            canvas,
            (0, 0, 255),
            ((self._agent_location[0] + 0.5) * pix_square_size_hor,(self._agent_location[1] + 0.5) * pix_square_size_ver), #position
            pix_square_size_ver / 3, # radius
        )



        # And the obstacles
        for i in range(len(self.obs_list)):
            for j in range(len(self.obs_list[i])):
                pygame.draw.rect(
                canvas,
                (0, 0, 0),
                 pygame.Rect(
                  (pix_square_size_hor  * self.obs_list[i][j][0],pix_square_size_ver  * self.obs_list[i][j][1]),
                  (pix_square_size_hor, pix_square_size_ver)
                  ),
        )

        # Finally, add some gridlines
        for y in range(self.size_ver + 1):
            pygame.draw.line(
                canvas,
                0,
                (0, pix_square_size_ver * y),
                (self.window_size_hor, pix_square_size_ver * y),
                width=3,
            )
        for x in range(self.size_hor + 1):
            pygame.draw.line(
                canvas,
                0,
                (pix_square_size_hor * x, 0),
                (pix_square_size_hor * x, self.window_size_hor),
                width=3,
            )



        if self.render_mode == "human":
            # The following line copies our drawings from `canvas` to the visible window
            self.window.blit(canvas, canvas.get_rect())
            pygame.event.pump()
            pygame.display.update()

            # We need to ensure that human-rendering occurs at the predefined framerate.
            # The following line will automatically add a delay to keep the framerate stable.
            self.clock.tick(0.001)
           
        else:  # rgb_array
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(canvas)), axes=(1, 0, 2)
            )

        
    # show trajectory
    def show_trajectory(self,traj):
        if self.window is None and self.render_mode == "human":
            pygame.init()
            pygame.display.init()
            self.window = pygame.display.set_mode((self.window_size_hor, self.window_size_ver))
        if self.clock is None and self.render_mode == "human":
            self.clock = pygame.time.Clock()

        canvas = pygame.Surface((self.window_size_hor, self.window_size_ver))
        canvas.fill((255, 255, 255))
        pix_square_size_hor = (
            self.window_size_hor / self.size_hor
        )  
        pix_square_size_ver = (
            self.window_size_ver / self.size_ver
        )  

        # Draw trajectory    
        font = pygame.font.SysFont('Arial', 15)
        for k in range(len(traj)):
            pygame.draw.rect(
            canvas,
            (0,0,255),
            pygame.Rect(
                (pix_square_size_hor  * traj[k][0],pix_square_size_ver  * traj[k][1]),
                (pix_square_size_hor, pix_square_size_ver)
                  ),
            )
            img = font.render(  str(k), True, (0,255,0))
            canvas.blit(img, (traj[k][0]*1*pix_square_size_hor,traj[k][1]*1*pix_square_size_ver))

        # Now we draw the agent
        pygame.draw.circle(
            canvas,
            (0, 0, 255),
            ((self._agent_location[0] + 0.5) * pix_square_size_hor,(self._agent_location[1] + 0.5) * pix_square_size_ver), #position
            pix_square_size_ver / 3, # radius
        )



        # And the obstacles
        for i in range(len(self.obs_list)):
            for j in range(len(self.obs_list[i])):
                pygame.draw.rect(
                canvas,
                (0, 0, 0),
                 pygame.Rect(
                  (pix_square_size_hor  * self.obs_list[i][j][0],pix_square_size_ver  * self.obs_list[i][j][1]),
                  (pix_square_size_hor, pix_square_size_ver)
                  ),
        )

        # Finally, add some gridlines
        for y in range(self.size_ver + 1):
            pygame.draw.line(
                canvas,
                0,
                (0, pix_square_size_ver * y),
                (self.window_size_hor, pix_square_size_ver * y),
                width=3,
            )
        for x in range(self.size_hor + 1):
            pygame.draw.line(
                canvas,
                0,
                (pix_square_size_hor * x, 0),
                (pix_square_size_hor * x, self.window_size_hor),
                width=3,
            )



        if self.render_mode == "human":
            # The following line copies our drawings from `canvas` to the visible window
            self.window.blit(canvas, canvas.get_rect())
            pygame.event.pump()
            pygame.display.update()

            # We need to ensure that human-rendering occurs at the predefined framerate.
            # The following line will automatically add a delay to keep the framerate stable.
            self.clock.tick(0.001)
           
        else:  # rgb_array
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(canvas)), axes=(1, 0, 2)
            )
    
    def _get_obs_list(self, id):
        match id:
            case 1:
                self.obs_list.append((0,4))
                self.obs_list.append((1,4))
                self.obs_list.append((0,5))
                self.obs_list.append((1,5))

                self.obs_list.append((2,4))
                self.obs_list.append((3,4))
                self.obs_list.append((2,5))
                self.obs_list.append((3,5))

                self.obs_list.append((10,0))
                self.obs_list.append((11,0))
                self.obs_list.append((10,1))
                self.obs_list.append((11,1))

                self.obs_list.append((10,6))
                self.obs_list.append((11,6))
                self.obs_list.append((10,7))
                self.obs_list.append((11,7))

                self.obs_list.append((12,6))
                self.obs_list.append((13,6))
                self.obs_list.append((12,7))
                self.obs_list.append((13,7))

                self.obs_list.append((4,10))
                self.obs_list.append((5,10))
                self.obs_list.append((4,11))
                self.obs_list.append((5,11))

                self.obs_list.append((4,12))
                self.obs_list.append((5,12))
                self.obs_list.append((4,13))
                self.obs_list.append((5,13))
            case 2:
                self.obs_list.append((0,5))
                self.obs_list.append((0,6))
                self.obs_list.append((0,7))
                self.obs_list.append((1,5))
                self.obs_list.append((1,6))
                self.obs_list.append((1,7))

                self.obs_list.append((12,5))
                self.obs_list.append((12,6))
                self.obs_list.append((12,7))
                self.obs_list.append((13,5))
                self.obs_list.append((13,6))
                self.obs_list.append((13,7))

                self.obs_list.append((4,3))
                self.obs_list.append((5,3))
                self.obs_list.append((6,3))
                self.obs_list.append((7,3))
                self.obs_list.append((8,3))
                self.obs_list.append((9,3))
                self.obs_list.append((4,4))
                self.obs_list.append((5,4))
                self.obs_list.append((6,4))
                self.obs_list.append((7,4))
                self.obs_list.append((8,4))
                self.obs_list.append((9,4))

                self.obs_list.append((4,8))
                self.obs_list.append((5,8))
                self.obs_list.append((6,8))
                self.obs_list.append((7,8))
                self.obs_list.append((8,8))
                self.obs_list.append((9,8))
                self.obs_list.append((4,9))
                self.obs_list.append((5,9))
                self.obs_list.append((6,9))
                self.obs_list.append((7,9))
                self.obs_list.append((8,9))
                self.obs_list.append((9,9))
            case 3:
                self.obs_list.append((6,0))
                self.obs_list.append((7,0))
                self.obs_list.append((8,0))
                self.obs_list.append((6,1))
                self.obs_list.append((7,1))
                self.obs_list.append((8,1))

                self.obs_list.append((6,12))
                self.obs_list.append((7,12))
                self.obs_list.append((8,12))
                self.obs_list.append((6,13))
                self.obs_list.append((7,13))
                self.obs_list.append((8,13))

                self.obs_list.append((4,4))
                self.obs_list.append((4,5))
                self.obs_list.append((4,6))
                self.obs_list.append((4,7))
                self.obs_list.append((4,8))
                self.obs_list.append((4,9))
                self.obs_list.append((5,4))
                self.obs_list.append((5,5))
                self.obs_list.append((5,6))
                self.obs_list.append((5,7))
                self.obs_list.append((5,8))
                self.obs_list.append((5,9))

                self.obs_list.append((9,4))
                self.obs_list.append((9,5))
                self.obs_list.append((9,6))
                self.obs_list.append((9,7))
                self.obs_list.append((9,8))
                self.obs_list.append((9,9))
                self.obs_list.append((10,4))
                self.obs_list.append((10,5))
                self.obs_list.append((10,6))
                self.obs_list.append((10,7))
                self.obs_list.append((10,8))
                self.obs_list.append((10,9))
            case 4:
                self.obs_list.append((4,4))
                self.obs_list.append((4,5))
                self.obs_list.append((4,6))
                self.obs_list.append((5,4))
                self.obs_list.append((5,5))
                self.obs_list.append((5,6))
                self.obs_list.append((6,4))
                self.obs_list.append((6,5))
                self.obs_list.append((6,6))

                self.obs_list.append((0,9))
                self.obs_list.append((1,9))
                self.obs_list.append((2,9))
                self.obs_list.append((3,9))
                self.obs_list.append((4,9))
                self.obs_list.append((5,9))
                self.obs_list.append((0,10))
                self.obs_list.append((1,10))
                self.obs_list.append((2,10))
                self.obs_list.append((3,10))
                self.obs_list.append((4,10))
                self.obs_list.append((5,10))

                self.obs_list.append((9,4))
                self.obs_list.append((10,4))
                self.obs_list.append((11,4))
                self.obs_list.append((12,4))
                self.obs_list.append((13,4))
                self.obs_list.append((9,5))
                self.obs_list.append((10,5))
                self.obs_list.append((11,5))
                self.obs_list.append((12,5))
                self.obs_list.append((13,5))

                self.obs_list.append((10,10))
                self.obs_list.append((10,11))
                self.obs_list.append((10,12))
                self.obs_list.append((10,13))
                self.obs_list.append((11,10))
                self.obs_list.append((11,11))
                self.obs_list.append((11,12))
                self.obs_list.append((11,13))
            case 5:
                self.obs_list.append((8,0))
                self.obs_list.append((8,1))
                self.obs_list.append((8,2))
                self.obs_list.append((8,3))
                self.obs_list.append((8,4))
                self.obs_list.append((8,5))
                self.obs_list.append((8,6))
                self.obs_list.append((8,7))
                self.obs_list.append((8,8))
                self.obs_list.append((8,9))
                self.obs_list.append((9,0))
                self.obs_list.append((9,1))
                self.obs_list.append((9,2))
                self.obs_list.append((9,3))
                self.obs_list.append((9,4))
                self.obs_list.append((9,5))
                self.obs_list.append((9,6))
                self.obs_list.append((9,7))
                self.obs_list.append((9,8))
                self.obs_list.append((9,9))

                self.obs_list.append((3,4))
                self.obs_list.append((3,5))
                self.obs_list.append((3,6))
                self.obs_list.append((3,7))
                self.obs_list.append((3,8))
                self.obs_list.append((3,9))
                self.obs_list.append((3,10))
                self.obs_list.append((3,11))
                self.obs_list.append((3,12))
                self.obs_list.append((3,13))
                self.obs_list.append((4,4))
                self.obs_list.append((4,5))
                self.obs_list.append((4,6))
                self.obs_list.append((4,7))
                self.obs_list.append((4,8))
                self.obs_list.append((4,9))
                self.obs_list.append((4,10))
                self.obs_list.append((4,11))
                self.obs_list.append((4,12))
                self.obs_list.append((4,13))
            case _:
                print("Invalid environment id")
                return []

        