import matplotlib
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.patches as mpatches
import matplotlib.animation as animation

import torch
import numpy as np
import pickle
import os
import glob
from PIL import Image
import gym_examples
import gym
import wandb
import shutil

# from airl import *
from airl import DiscriminatorMLP, Discriminator, update_discriminator
from ppo import PPO, TrajectoryDataset, update_policy
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.utils import set_random_seed
from tqdm import tqdm


# Device Check
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class AnalyseResults:
    def __init__(self, env_x , env_y):
        self.env_x = env_x
        self.env_y = env_y

    def get_snapshots(self, path, traj, actions):
        # env.state: 0 agent, 1 target A, 2 target B, 3 obstacles, 4 terminal state

        cmap = ['r', 'c', 'b', 'k', 'y']

        # A trajecetory exists out of a number (k) of states (t), the model moving from
        # beginning to terminal state
        for k, t in enumerate(traj):
            fig = plt.figure()
            ax = fig.add_subplot(111)

            # Give all non empty tiles their relevant colour in the grid
            for p in np.flip(np.argwhere(t>0), 0):
                state_coords = matplotlib.patches.Rectangle((p[2], p[1]), 1, 1, color=cmap[p[0]])
                ax.add_patch(state_coords)

            action_names = ['r', 'dr', 'd', 'dl', 'l', 'ul', 'u', 'ur', 'C']

            # Create grid of respective size
            plt.xlim([0, self.env_x])
            plt.ylim([0, self.env_y])
            plt.title('Action: '+action_names[actions[k]])
            plt.grid()

            # Shrink current axis's height by 10% on the bottom
            box = ax.get_position()
            ax.set_position([box.x0, box.y0 + box.height * 0.1,
                             box.width, box.height * 0.9])

            # Put a legend below current axis
            ax.legend(loc='upper center', handles=[mpatches.Patch(color='r', label='Agent'), mpatches.Patch(color='c', label='Target A'), \
                    mpatches.Patch(color='b', label='Target B'), mpatches.Patch(color='k', label='Obstacle'), \
                    mpatches.Patch(color='y', label='Terminal')], bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=5)

            # If directory exists save the grid, otherwise create dir and save
            if not os.path.exists(path):
                os.makedirs(path)
            
            plt.savefig(path + str(k) + '_' + action_names[actions[k]] + '.png', format='png', dpi=1200)

            plt.close()


    def make_animation(self, path, save_path, t):
        # After saving all states, create gif
        imgs = []
        for img in sorted(glob.glob(path+'*.png'), key=os.path.getmtime):
            imgs.append(Image.open(img))

        fig, ax = plt.subplots()
        im = ax.imshow(imgs[0], animated=True)

        def update(i):
            im.set_array(imgs[i])
            return im

        # Create animation of array of grids previously saved
        animation_fig = animation.FuncAnimation(fig, update, frames=len(imgs), interval=200, blit=False, repeat_delay=10)

        # If directory exists save the gif, otherwise create dir and save
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        animation_fig.save(save_path + "traj_" + str(t) + ".gif")

        plt.close()


    def animate_expert_traj(self, version, save_path, n=0):

        # Load the export trajectories
        expert_trajectories = pickle.load(open('./demos/expert_policy_' + version + '.pk', 'rb')) 
        traj = expert_trajectories[n] #[:, 0, :, :]

        # Create grids and gifs based on trajectories
        self.get_snapshots(save_path+str(n)+'/', traj['states'], traj['actions'])
        self.make_animation(save_path+str(n)+'/', save_path+'gifs/', n)


    def train_model(self, version, attempt="", create_gif=True, log=False, save_policy=False):
        # REQUIRED FOR MULTI-THREADING
        def make_env(env_id, rank, seed=0):
            """
            Utility function for multiprocessed env.
        
            :param env_id: (str) the environment ID
            :param seed: (int) the inital seed for RNG
            :param rank: (int) index of the subprocess  
            """
            def _init():
                env = gym.make(env_id)

                # Important: use a different seed for each environment
                env.seed(seed + rank)
                return env
            set_random_seed(seed)
            return _init

        # Load demonstrations
        expert_trajectories = pickle.load(open('./demos/expert_policy_' + version + '.pk', 'rb')) 

        # Init Parameters
        config={
            'env_id': 'small_multitarget_v0',
            # 'env_steps': 3500000,
            'env_steps': 16000000,
            'n_workers': 8, 
            'batchsize_discriminator': 128, 
            'batchsize_ppo': 16, 
            'entropy_reg': 0.018936457902814,
            'gamma': 0.9459325302510204,
            'epsilon': 0.1421513921427619,
            'ppo_epochs': 4
        }

        # Include option to run offline and only create gifs
        if log: current_run = wandb.init(project='AIRL_'+version, config=config)

        # Create Environment
        vec_env = SubprocVecEnv([make_env("gym_examples/SmallMuseumCollect-v0", i) for i in range(config['n_workers'])]) 

        states = vec_env.reset()
        states_tensor = torch.tensor(states).float().to(device)

        # Fetch Shapes
        n_actions = vec_env.action_space.n
        obs_shape = vec_env.observation_space.shape
        state_shape = obs_shape[1:]
        in_channels = obs_shape[0]

        # Initialize Models
        ppo = PPO(state_shape=state_shape, n_actions=n_actions, in_channels=in_channels).to(device)
        discriminator = DiscriminatorMLP(state_shape=state_shape, in_channels=in_channels).to(device)

        optimizer_lr=0.00042554675239701655
        discriminator_lr=0.00041208405199317534
        optimizer = torch.optim.Adam(ppo.parameters(), lr=optimizer_lr) # 4e-4
        optimizer_discriminator = torch.optim.Adam(discriminator.parameters(), lr=discriminator_lr) # 4e-4
        dataset = TrajectoryDataset(batch_size=config['batchsize_ppo'], n_workers=config['n_workers'])

        # Logging
        objective_logs = []

        for t in tqdm(range((int(config['env_steps']/config['n_workers'])))):

            # Sample action
            actions, log_probs = ppo.act(states_tensor)

            next_states, rewards, dones, _  = vec_env.step(actions)

            # Data treatment
            next_states = np.stack(next_states, axis=0) # correct format

            # Log Objectives
            objective_logs.append(rewards)

            # Calculate (vectorized) AIRL reward
            airl_state = torch.tensor(states).to(device).float()
            airl_next_state = torch.tensor(next_states).to(device).float()
            airl_action_prob = torch.exp(torch.tensor(log_probs)).to(device).float()

            for i in range(config['n_workers']):
                #"""
                if not dones[i]:
                    airl_rewards = discriminator.predict_reward(airl_state, airl_next_state, config['gamma'], airl_action_prob) # \hat{r}
                else: # overwrittes reset from env to register zeroed reward matrix
                    state_aux = states.copy()
                    state_aux[0][1] = np.zeros((self.env_y,self.env_x))
                    state_aux[0][2] = np.zeros((self.env_y,self.env_x))
                    test_airl_state = torch.tensor(state_aux).to(device).float()
                    airl_rewards = discriminator.predict_reward(airl_state, test_airl_state, config['gamma'], airl_action_prob)
                #"""

            airl_rewards = list(airl_rewards.detach().cpu().numpy() * [0 if i else 1 for i in dones])

            # Save Trajectory
            train_ready = dataset.write_tuple(states, actions, airl_rewards, dones, log_probs, rewards) # GENERATOR trajectories

            if train_ready:
                
                #Hack: the movement penalization log keeps registering penalizations after the done signal (only a logging problem)
                objective_logs = dataset.log_objectives()
                for j in range(config['batchsize_ppo']):
                    traj_length = len(dataset.trajectories[j]['rewards'])
                    objective_logs[j, 0] = rewards[0,0]*traj_length
                # END OF hack SOLUTION

                if log:
                    current_run.log({'Movement penalties' : objective_logs[:, 0].mean()})
                    current_run.log({'Collision penalties' : objective_logs[:, 3].mean()})
                    current_run.log({'Target A rewards' : objective_logs[:, 1].mean()})
                    current_run.log({'Target B rewards' : objective_logs[:, 2].mean()})

                objective_logs = []

                # Update Models
                update_policy(ppo, dataset, optimizer, config['gamma'], config['epsilon'], config['ppo_epochs'],
                              entropy_reg=config['entropy_reg'])
                d_loss, fake_acc, real_acc = update_discriminator(discriminator=discriminator,
                                                              optimizer=optimizer_discriminator,
                                                              gamma=config['gamma'],
                                                              expert_trajectories=expert_trajectories,
                                                              policy_trajectories=dataset.trajectories.copy(), ppo=ppo,
                                                              batch_size=config['batchsize_discriminator'])
                
                if log:
                    # Log Loss Statsitics
                    current_run.log({'Discriminator Loss': d_loss,
                            'Fake Accuracy': fake_acc,
                            'Real Accuracy': real_acc})
                    
                    for ret in dataset.log_returns():
                        current_run.log({'Returns': ret})

                # Create gif of model's actions every 10,000 itterations to see 
                # the progress during training
                if (t % 100000 <= 200) and create_gif:
                    tau = dataset.trajectories[0] 
                    self.get_snapshots('./snapshots/exp_'+version+attempt+'/batch_'+str(0)+'/itteration_'+str(t)+'/', tau['states'], tau['actions'])
                    self.make_animation('./snapshots/exp_'+version+attempt+'/batch_'+str(0)+'/itteration_'+str(t)+'/', \
                                            './snapshots/exp_'+version+attempt+'/batch_'+str(0)+'/gifs/', t)
                    print(f'Created batch of gifs for itteration {t}')

                dataset.reset_trajectories()

            # This is a continuation of the sampling part
            # Prepare state input for next time step
            if not train_ready:
                states = next_states.copy()
                states_tensor = torch.tensor(states).float().to(device)

        vec_env.close()
        if log: current_run.finish()
        if save_policy: torch.save(ppo.state_dict(), "./saved_policies/airl_policy_" + version + attempt + ".pt")

        # Delete local wandb run to save storage
        for f in os.listdir("./wandb"):
            if(f[:3] == 'run'):
                shutil.rmtree("./wandb/" + f)

if __name__ == '__main__':

    version = 'random_env_max'
    attempt = '' 

    A = AnalyseResults(env_x=14, env_y=14)
    A.train_model(version=version, attempt=attempt, create_gif=True, log=True, save_policy=True)

    # for i in range(30):
    #     A.animate_expert_traj(version, './snapshots/exp_traj_'+version+'/', i)