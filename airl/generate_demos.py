from tqdm import tqdm
from ppo import *
import torch
from matplotlib import pyplot as plt
import matplotlib.patches as patches
from airl import *
import gym
import gym_examples
import pickle

# Use GPU if available
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

if __name__ == '__main__':
    
    # Create Environment
    env_id = 'SmallMuseumCollect-v0'
    env = gym.make("gym_examples/"+env_id)
    states = env.reset()
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch Shapes
    n_actions = env.action_space.n
    obs_shape = env.observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]

    # Select n demos (need to set timer separately)
    n_demos = 8000
    max_steps = 45

    # PPO
    expert_policy_file= 'expert_policy_random_env_max'
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    ppo.load_state_dict(torch.load('./saved_policies/' + expert_policy_file + '.pt'))
    
    dataset = []
    episode = {'states': [], 'actions': [], 'terminal': []}

    while len(dataset) <= n_demos: 
        if len(dataset)%100 == 0 and  len(dataset)%100 > 0:
            print(len(dataset))

        return_ = 0
        # Dynamics
        for k in range(max_steps):
            actions, log_probs = ppo.act(states_tensor)
            next_states, rewards, done, _ = env.step(actions[0]) # done is related to 'k' (states) not 'k+1' (next_states)
            next_states = np.stack(next_states, axis=0)
            episode['states'].append(states)
            episode['actions'].append(actions) # Note: Actions currently append as arrays and not integers!
            episode['terminal'].append(env.terminal_location)

            return_ = return_ + np.sum(rewards)
            
            # termination
            if done: 
                dataset.append(episode)
                print(len(dataset))
                    
                episode = {'states': [], 'actions': [], 'terminal': []}
                states = env.state.copy()
                states_tensor = torch.tensor(states).float().to(device)
                break

            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)


    pickle.dump(dataset, open('./demos/' + expert_policy_file + '.pk', 'wb'))

    


   

